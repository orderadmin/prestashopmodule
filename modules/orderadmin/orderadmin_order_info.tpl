{if ($ps_version == '1.6') OR ($ps_version == '1.7')}
<div class="col-lg-7">
    <div class="panel" style="padding-bottom:50px;">
        <div class="panel-heading">
            <span><img src="{$this_path}oa_lit.png" /> {l s='Информация за поръчката' mod='orderadmin'}</span>
        </div>
        <div id="orderadmin" class="well hidden-print">
{else}
<fieldset>
{/if}
    <table class="orderadmin_generate" id="orderadmin_tabel">
        <tr>
            <td><label for="order_id">{l s='ID на поръчката:' mod='orderadmin'}</label></td>
            <td>
                <input type="text" id="order_id" name="order_id" disabled="disabled" value="{$order_id}" />
            </td>
        </tr>

        <tr>
            <td><label for="oa_order_id">{l s='ID на поръчката в Orderadmin:' mod='orderadmin'}</label></td>
            <td>
                <input type="text" id="oa_order_id" name="oa_order_id" disabled="disabled" value="{$oa_order_id}" />
                <br />
            </td>
        </tr>

        <tr>
            <td><label for="dr_id">{l s='ID на заявката в Orderadmin:' mod='orderadmin'}</label></td>
            <td>
                <input type="text" id="dr_id" name="dr_id" disabled="disabled" value="{$dr_id}" />
                <br />
            </td>
        </tr>

        <tr>
            <td><label for="order_state">{l s='Статус на поръчката:' mod='orderadmin'}</label></td>
            <td>
                <input type="text" id="order_state" name="order_state" disabled="disabled" value="{$order_state}" />
                <br />
            </td>
        </tr>

        {if !$dr_id && $oa_order_id}

            <tr class="oa_hide">
                <td><label for="delivery_to">{l s='Поръчка до:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" id="delivery_to" name="delivery_to" disabled="disabled" value="{if !$speedy_office_id}{l s='адрес' mod='orderadmin'}{else}{l s='офис' mod='orderadmin'}{/if}" />
                    <br />
                </td>
            </tr>

            <tr class="oa_hide">
                <td><label for="delivery_city">{l s='Град:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" id="delivery_city" name="delivery_city" disabled="disabled" value="{$deliveryCity}" />
                    <br />
                </td>
            </tr>

            <tr class="oa_hide">
                <td><label for="delivery_postcode">{l s='Пощенски код:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" id="delivery_postcode" name="delivery_postcode" disabled="disabled" value="{$deliveryCityPostCode}" />
                    <br />
                </td>
            </tr>

            {if $speedyOfficeAddress}
            <tr class="oa_hide">
                <td><label for="delivery_address">{l s='Адрес:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" id="delivery_address" name="delivery_address" disabled="disabled" value="{$speedyOfficeAddress}" />
                    <br />
                </td>
            </tr>
            {/if}

            {if $trackingNumber}
                <tr class="oa_hide">
                    <td><label for="trackingNumber">{l s='Товарителница:' mod='orderadmin'}</label></td>
                    <td>
                        <input type="text" id="trackingNumber" name="trackingNumber" disabled="disabled" value="{$trackingNumber}" />
                        <br />
                    </td>
                </tr>
            {/if}

        {/if}
    </table>
{if ($ps_version == '1.6') OR ($ps_version == '1.7')}
        </div>
        {if !$dr_id && $oa_order_id}
            <div class="buttons oa_hide">
                <button type="submit" id="createDr" class="btn btn-primary pull-left">{l s='Създай заявка в Orderadmin' mod='orderadmin'}</button>
            </div>
        {/if}
    </div>
</div>
{else}
</fieldset>
{/if}

<script type="text/javascript">
    {literal}

    $('button#createDr').click(function(e){

        var postdata = {
            ajax:           true,
            controller:     'adminOrders',
            action:         'createDeliveryRequest',
            token:          '{/literal}{$ordersToken}{literal}',
            oa_order_id:    '{/literal}{$oa_order_id}{literal}',
            order_id:       '{/literal}{$order_id}{literal}',
            point_id:       '{/literal}{$speedy_office_id}{literal}',
            point_addr:     '{/literal}{$speedyOfficeAddress}{literal}',
            loc_name:       '{/literal}{$deliveryCity}{literal}',
            loc_pc:         '{/literal}{$deliveryCityPostCode}{literal}',
            loc_id:         '{/literal}{$loc_id}{literal}',
            trackingNumber: '{/literal}{$trackingNumber}{literal}',
        };

        $.ajax({
            type: 'POST',
            url: 'index.php',
            dataType: 'json',
            data: postdata,
            success: function(data){
                if(data.error) {
                    alert('{/literal}{l s='Появи се проблем при обработката на заявката' mod='orderadmin'}{literal}');
                    $('input[type=radio]').prop('checked', false);
                } else {
                    $('input#dr_id').val(data.drId);
                    $('.oa_hide').hide();
                }
            }
        });
    });

    {/literal}
</script>