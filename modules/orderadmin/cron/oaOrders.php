<?php
require_once(_PS_MODULE_DIR_.'orderadmin/orderadmin.php');

class OaOrders extends Orderadmin
{
    //CRON
    public function initContent()
    {
        $sql = sprintf(
            'SELECT * FROM %sorderadmin_order_products_awaiting WHERE `execution_time` IS NULL AND `result` IS NULL LIMIT 1',
            _DB_PREFIX_
        );

        $dbRes = Db::getInstance()->ExecuteS($sql);

        if (empty($dbRes)) {
            echo "There are no jobs in the table\n";

            return;
        }
        $data = $dbRes[0];

        if(!empty($data)){
            switch ($data['type']) {
                case self::TYPE_POST:
                    $orderProductData = $data['data'];

                    $this->postToOrderadmin(
                        $orderProductData,
                        'api/products/order/product',
                        self::TYPE_POST,
                        false,
                        $data['orderadmin_order_products_awaiting_id']
                    );
                    break;

                case self::TYPE_PATCH:
                    $arr = (array)json_decode($data['data']);
                    $prodData = json_encode($arr['orderProductData']);

                    $this->postToOrderadmin(
                        $prodData,
                        'api/products/order/product/' . $arr['id'],
                        self::TYPE_PATCH,
                        false,
                        $data['orderadmin_order_products_awaiting_id']
                    );
                    break;

                case self::TYPE_DELETE:
                    $arr = (array)json_decode($data['data']);

                    $this->postToOrderadmin(
                        [],
                        'api/products/order/product/' . $arr['id'],
                        self::TYPE_DELETE,
                        false,
                        $data['orderadmin_order_products_awaiting_id']
                    );
                    break;
            }

            echo sprintf(
                'Order product %sED',
                $data['type']
            );
        }
    }
}