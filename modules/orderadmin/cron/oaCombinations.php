<?php
require_once(_PS_MODULE_DIR_.'orderadmin/orderadmin.php');

class OaCombinations extends Orderadmin
{
    //CRON
    public function initContent()
    {
        $sql = sprintf(
            'SELECT attr_id AS attr, product_id AS product FROM %sorderadmin_awaiting where product_id = 54230 LIMIT 1',
            _DB_PREFIX_
        );

        $dbRes = Db::getInstance()->ExecuteS($sql);

        if (empty($dbRes))
        {
            echo "There are no jobs in the table\n";
            return null;
        }

        if(!empty($dbRes[0]['attr']) && !empty($dbRes[0]['product'])) {

            $id_lang = (int)$this->context->language->id;

            $product = new Product((int)$dbRes[0]['product'], false, $id_lang);

            $att = $product->getAttributeCombinationsById((int)$dbRes[0]['attr'],$id_lang);

            $img = $product->getCombinationImageById((int)$dbRes[0]['attr'],$id_lang);

            $simpleOfferExtId = sprintf(
                '%s-%s',
                $att[0]['id_product'],
                $att[0]['id_product_attribute']
            );

            $simpleOfferData = [
                'image' => ! empty($img) ? $img : '',
                'name' => $product->name,
                'noTask' => 'true',
                'weight' => ! empty($att['weight'])
                    ? round($att['weight'], 2) : round($product->weight, 2),
                'sku' => ! empty($att[0]['reference'])
                    ? $att[0]['reference'] : null,
                'article' => ! empty($att[0]['reference'])
                    ? $att[0]['reference'] : null,
                'price' => (intval($att[0]['price']) != 0) ? round(
                    $att[0]['price'] + $product->price, 2) : round($product->price, 2),
                'purchasingPrice' => (intval($att[0]['wholesale_price']) != 0) ? round(
                    $att[0]['wholesale_price'], 2) : round($product->wholesale_price, 2),
                'barcodes' => [
                    $att[0]['ean13']
                ]
            ];

            $oaSimpSql = sprintf(
                'SELECT product_extId FROM %sorderadmin_products WHERE product_id = "%s"',
                _DB_PREFIX_,
                trim($simpleOfferExtId)
            );

            $oaSimpleProductExtid = Db::getInstance()->getValue($oaSimpSql);

            if (!empty($oaSimpleProductExtid)) {

                $this->postToOrderadmin(
                    $simpleOfferData,
                    'api/products/offer/' . (int)$oaSimpleProductExtid,
                    self::TYPE_PATCH
                );

            } else {
                $shopSql = sprintf(
                    'SELECT value FROM %sconfiguration WHERE name = "%s"',
                    _DB_PREFIX_,
                    trim('ORDERADMIN_DEFAULT_SHOP')
                );

                $shopId = Db::getInstance()->getValue($shopSql);

                $oaSimpleData = $this->getData(
                    'api/products/offer',
                    'product_offer',
                    [
                        'filter' => [
                            [
                                'field' => 'shop',
                                'type' => 'eq',
                                'value' => $shopId
                            ],
                            [
                                'field' => 'extId',
                                'type' => 'eq',
                                'value' => $simpleOfferExtId
                            ],
                        ]
                    ]
                );

                if (!empty($oaSimpleData['id'])) {

                    $this->postToOrderadmin(
                        $simpleOfferData,
                        'api/products/offer/' . (int)$oaSimpleData['id'],
                        self::TYPE_PATCH
                    );
                } else {
                    $parentSql = sprintf(
                        'SELECT product_extId FROM %sorderadmin_products WHERE product_id = "%s"',
                        _DB_PREFIX_,
                        $att[0]['id_product']
                    );

                    $parentId = Db::getInstance()->getValue($parentSql);

                    $simpleOfferData['shop'] = $shopId;
                    $simpleOfferData['extId'] = $simpleOfferExtId;
                    $simpleOfferData['type'] = 'simple';
                    $simpleOfferData['parent'] = (int)$parentId;
                    $simpleOfferData['state'] = 'normal';

                    if(count($att) > 1) {
                        $attName = [];
                        foreach ($att as $attribute)
                            $attName[] = $attribute['attribute_name'];
                        if (! count($attName))
                            return '-';
                        $simpleOfferData['name'] =  implode(' - ', $attName);
                    } else {
                        $simpleOfferData['name'] = sprintf(
                            '%s/%s - %s',
                            $product->name,
                            $att[0]['group_name'],
                            $att[0]['attribute_name']
                        );
                    }

                    $this->postToOrderadmin(
                        $simpleOfferData,
                        'api/products/offer',
                        self::TYPE_POST
                    );
                }
            }

            $oaSimpSql = sprintf(
                'SELECT product_extId FROM %sorderadmin_products WHERE product_id = "%s"',
                _DB_PREFIX_,
                trim($simpleOfferExtId)
            );

            $oaSimpleProductExtid = Db::getInstance()->getValue($oaSimpSql);

            if (!empty($oaSimpleProductExtid)) {
                $delSql = sprintf(
                    'DELETE FROM %sorderadmin_awaiting WHERE attr_id = %s',
                    _DB_PREFIX_,
                    (int)$dbRes[0]['attr']
                );

                Db::getInstance()->Execute($delSql);
                echo 'Combination sent to Orderadmin';
            } else {
                echo sprintf(
                    'There was problem with saving combination %s for offer %s in Orderadmin',
                    $att[0]['id_product_attribute'],
                    $att[0]['id_product']
                );
            }
        }
        return null;
    }
}