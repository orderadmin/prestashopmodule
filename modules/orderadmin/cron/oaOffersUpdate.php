<?php
require_once(_PS_MODULE_DIR_.'orderadmin/orderadmin.php');

class OaOffersUpdate extends Orderadmin
{
    //CRON
    public function initContent()
    {
        $sql = sprintf(
            'SELECT p.id_product,
                pl.name AS "name",
                p.price AS "price",
                p.wholesale_price AS "wholesale_price",
                p.ean13 AS "ean13",
                p.weight AS "weight",
                p.reference AS "reference",
                p.date_add AS "date_created",
                p.date_upd AS "date_updated",
                GROUP_CONCAT(pa.id_product_attribute SEPARATOR ",") as "attr_id",
                CONCAT(
                    \'https://www.baby.bg/img/p/\',
                    IF(CHAR_LENGTH(pi.id_image) >= 7, CONCAT(SUBSTRING(pi.id_image, -7, 1), \'/\'), \'\'),
                    IF(CHAR_LENGTH(pi.id_image) >= 6, CONCAT(SUBSTRING(pi.id_image, -6, 1), \'/\'), \'\'),
                    IF(CHAR_LENGTH(pi.id_image) >= 5, CONCAT(SUBSTRING(pi.id_image, -5, 1), \'/\'), \'\'),
                    IF(CHAR_LENGTH(pi.id_image) >= 4, CONCAT(SUBSTRING(pi.id_image, -4, 1), \'/\'), \'\'),
                    IF(CHAR_LENGTH(pi.id_image) >= 3, CONCAT(SUBSTRING(pi.id_image, -3, 1), \'/\'), \'\'),
                    if(CHAR_LENGTH(pi.id_image) >= 2, CONCAT(SUBSTRING(pi.id_image, -2, 1), \'/\'), \'\'),
                    IF(CHAR_LENGTH(pi.id_image) >= 1, CONCAT(SUBSTRING(pi.id_image, -1, 1), \'/\'), \'\'),
                    pi.id_image, 
                    \'.jpg\'
                ) as "image_url"
                FROM %sproduct p
                LEFT JOIN %sproduct_lang pl ON p.id_product = pl.id_product AND id_shop = 1 and id_lang = 1
                LEFT JOIN %sproduct_attribute pa ON p.id_product = pa.id_product and id_lang = 1
                LEFT JOIN %simage pi ON p.id_product = pi.id_product AND cover = 1
                WHERE p.date_upd > DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                AND p.active = 1
                GROUP BY p.id_product',
            _DB_PREFIX_,
            _DB_PREFIX_,
            _DB_PREFIX_,
            _DB_PREFIX_
        );

        $offers = Db::getInstance()->ExecuteS($sql);

        if (empty($offers)) {
            $return = [
                'message' => sprintf(
                    'No products have been found with params limit %s and offset, updated since %s',
                    500,
                    0,
                    date(
                        'Y-m-d h:m:s',
                        strtotime('- 1 day')
                    )
                )
            ];

            die(
            Tools::jsonEncode(
                $return
            )
            );
        }

        foreach ($offers as $key => $offer) {
            if(empty($offer['attr_id'])) {
                continue;
            }

            $comb = Db::getInstance()->executeS('
                SELECT pac.id_product_attribute, 
                        pa.price as "combination_price",
                        pa.ean13 as "combination_ean13",
                        pa.weight as "combination_weight",
                        pa.reference as "combination_reference",
                        pa.wholesale_price as "combination_wholesale_price",
                        GROUP_CONCAT(agl.`name`, " - ",al.`name` ORDER BY agl.`id_attribute_group` SEPARATOR " - ") as combination_name
                FROM `' . _DB_PREFIX_ . 'product_attribute_combination` pac
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute` a ON a.`id_attribute` = pac.`id_attribute`
                LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa ON pa.`id_product_attribute` = pac.`id_product_attribute`
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group` ag ON ag.`id_attribute_group` = a.`id_attribute_group`
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = 1)
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = 1)
                WHERE pac.id_product_attribute IN (' . $offer['attr_id'] . ')
                GROUP BY pac.id_product_attribute');

            $offers[$key]['combinations'] = $comb;

        }

//        echo "<pre>";
//        echo "<b>".__FILE__."</b><br/>";
//        var_dump(count($offers));
//        echo "</pre>";
//        die();

        die(
            Tools::jsonEncode(
                $offers
            )
        );
    }
}