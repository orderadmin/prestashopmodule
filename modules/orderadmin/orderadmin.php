<?php
if (!defined('_PS_VERSION_'))
    exit;

class Orderadmin extends Module {

    const MODULE_NAME = 'orderadmin';
    const TYPE_GET = 'GET';
    const TYPE_POST = 'POST';
    const TYPE_PATCH = 'PATCH';
    const TYPE_DELETE = 'DELETE';

    private $error_arr = array();
    protected $result;

    public function __construct() {
        $this->name = self::MODULE_NAME;
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.0';
        $this->author = 'Orderadmin';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => 1.5, 'max' => _PS_VERSION_);

        parent::__construct();

        $this->displayName = $this->l('Orderadmin');
        $this->description = $this->l('Модул за доставки със Orderadmin.');

        $this->confirmUninstall = $this->l('Сигурни ли сте, че искате да деинсталирате този модул?');
    }

    public function install() {
        $carrierConfig = array(
            'name' => 'Orderadmin',
            'id_tax_rules_group' => 0,
            'url' => 'http://alpha.orderadmin.ru',
            'active' => false,
            'deleted' => 0,
            'shipping_handling' => false,
            'range_behavior' => 0,
            'delay' => array(
                'bg' => 'Бърза доставка със Orderadmin!',
                'en' => 'Fast delivery with Orderadmin!',
                Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')) => 'Orderadmin delivery service module!'
            ),
            'id_zone' => 1, // Area where the carrier operates
            'is_module' => true, // We specify that it is a module
            'shipping_external' => true,
            'external_module_name' => 'orderadmin', // We specify the name of the module
            'need_range' => true
        );

        if (version_compare(@constant('_PS_VERSION_'), '1.7', '>=')) {
            $carrierConfig['name'] = 'Orderadmin';
        }

        $id_carrier = $this->installExternalCarrier($carrierConfig);

        Configuration::updateValue('ORDERADMIN_CARRIER_ID', (int) $id_carrier);

        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (
            !parent::install() OR
            !$this->registerHook('backOfficeHeader') OR
            !$this->registerHook('updateCarrier')
        )
            return false;

        Configuration::updateValue('ORDERADMIN_MODULE', 'Orderadmin');


        include_once(dirname(__FILE__) . '/sql/sql-install.php');

        foreach ($sql as $s)
            if (!Db::getInstance()->Execute($s))
                return false;

        return true;
    }

    public function uninstall() {
        if (
            !parent::uninstall() ||
            !Configuration::deleteByName('ORDERADMIN_MODULE') ||
            !$this->unregisterHook('backOfficeHeader') ||
            !$this->unregisterHook('updateCarrier')
        )
            return false;

        include_once(dirname(__FILE__) . '/sql/sql-uninstall.php');
        foreach ($sql as $s)
            if (!Db::getInstance()->Execute($s))
                return false;

        Configuration::deleteByName('ORDERADMIN_SERVER_ADDRESS');
        Configuration::deleteByName('ORDERADMIN_USERNAME');
        Configuration::deleteByName('ORDERADMIN_PASSWORD');
        Configuration::deleteByName('ORDERADMIN_OWNER');
        Configuration::deleteByName('ORDERADMIN_SENDER');
        Configuration::deleteByName('ORDERADMIN_DEFAULT_SHOP');
        Configuration::deleteByName('ORDERADMIN_DEFAULT_SOURCE');
        Configuration::deleteByName('ORDERADMIN_SECRET');
        Configuration::deleteByName('ORDERADMIN_HOOK_ORDER');
        Configuration::deleteByName('ORDERADMIN_HOOK_ORDER_EDIT');
        Configuration::deleteByName('ORDERADMIN_HOOK_ORDER_STATUS');
        Configuration::deleteByName('ORDERADMIN_HOOK_PRODUCT');
        Configuration::deleteByName('ORDERADMIN_HOOK_ADDRESS');

        $carrier = new Carrier((int) (Configuration::get('ORDERADMIN_CARRIER_ID')));
        $carrier->deleted = 1;
        if (!$carrier->update())
            return false;

        Configuration::deleteByName('ORDERADMIN_CARRIER_ID');

        return true;
    }

    private function installExternalCarrier($config) {
        $carrier = new Carrier();
        $carrier->name = $config['name'];
        $carrier->url = $config['url'];
        $carrier->id_tax_rules_group = $config['id_tax_rules_group'];
        $carrier->id_zone = $config['id_zone'];
        $carrier->active = $config['active'];
        $carrier->deleted = $config['deleted'];
        $carrier->delay = $config['delay'];
        $carrier->shipping_handling = $config['shipping_handling'];
        $carrier->range_behavior = $config['range_behavior'];
        $carrier->is_module = $config['is_module'];
        $carrier->shipping_external = $config['shipping_external'];
        $carrier->external_module_name = $config['external_module_name'];
        $carrier->need_range = $config['need_range'];

        $languages = Language::getLanguages(true);
        foreach ($languages as $language) {
            if ($language['iso_code'] == 'fr')
                $carrier->delay[(int) $language['id_lang']] = $config['delay'][$language['iso_code']];
            if ($language['iso_code'] == 'en')
                $carrier->delay[(int) $language['id_lang']] = $config['delay'][$language['iso_code']];
            if ($language['iso_code'] == Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')))
                $carrier->delay[(int) $language['id_lang']] = $config['delay'][$language['iso_code']];
        }

        if ($carrier->add()) {
            $groups = Group::getGroups(true);
            foreach ($groups as $group)
                Db::getInstance()->insert('carrier_group', array('id_carrier' => (int)($carrier->id), 'id_group' => (int)($group['id_group'])));

            $rangePrice = new RangePrice();
            $rangePrice->id_carrier = $carrier->id;
            $rangePrice->delimiter1 = '0';
            $rangePrice->delimiter2 = '10000';
            $rangePrice->add();

            $rangeWeight = new RangeWeight();
            $rangeWeight->id_carrier = $carrier->id;
            $rangeWeight->delimiter1 = '0';
            $rangeWeight->delimiter2 = '10000';
            $rangeWeight->add();

            $zones = Zone::getZones(true);
            foreach ($zones as $zone) {
                Db::getInstance()->insert('carrier_zone', array('id_carrier' => (int)($carrier->id), 'id_zone' => (int)($zone['id_zone'])));
                Db::getInstance()->insert('delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => (int)($rangePrice->id), 'id_range_weight' => NULL, 'id_zone' => (int)($zone['id_zone']), 'price' => '0'));

                Db::getInstance()->insert('delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => NULL, 'id_range_weight' => (int)($rangeWeight->id), 'id_zone' => (int)($zone['id_zone']), 'price' => '0'));
            }

            return (int) ($carrier->id);
        }

        return false;
    }

    //update module settings
    public function hookUpdateCarrier($params) {
        if ((int) ($params['id_carrier']) == (int) (Configuration::get('ORDERADMIN_CARRIER_ID')))
            Configuration::updateValue('ORDERADMIN_CARRIER_ID', (int) ($params['carrier']->id));
    }

    //add styles to backend admin module config page
    public function hookBackOfficeHeader($params) {

        if (method_exists($this->context->controller, "addCSS")) {
            $this->context->controller->addCSS(($this->_path).'orderadmin.css', 'all');
        }

        if (method_exists($this->context->controller, "addJS")) {
        }
    }

    //backend admin view
    public function getContent() {
        if (Tools::getValue('ajax_function') == 'import_data') {
            $this->importOrderadminData();

            //run CRON test method manually
            //$this->testComb();
            //$this->testOrder();
        } elseif (Tools::getValue('ajax_function') == 'register_hook') {
            Configuration::updateValue(Tools::getValue('config'), 1);
            $this->registerHook(Tools::getValue('hook'));

            if(
                Tools::getValue('hook') == 'actionValidateOrder' ||
                Tools::getValue('hook') == 'actionOrderEdited' ||
                Tools::getValue('hook') == 'actionOrderStatusPostUpdate'
            )
            {
                $this->registerHook('displayAdminOrderLeft');
            } elseif (Tools::getValue('hook') == 'actionProductSave') {
                $this->registerHook('displayAdminProductsExtra');
            }

            return null;
        } elseif (Tools::getValue('ajax_function') == 'unregister_hook') {
            Configuration::updateValue(Tools::getValue('config'), 0);
            $this->unregisterHook(Tools::getValue('hook'));

            if(
                Tools::getValue('hook') == 'actionValidateOrder' ||
                Tools::getValue('hook') == 'actionOrderEdited' ||
                Tools::getValue('hook') == 'actionOrderStatusPostUpdate'
            ){
                $this->unregisterHook('displayAdminOrderLeft');
            } elseif (Tools::getValue('hook') == 'actionProductSave') {
                $this->unregisterHook('displayAdminProductsExtra');
            }

            return null;
        }

        if (version_compare(@constant('_PS_VERSION_'), '1.7', '>=')) {
            $this->context->smarty->assign('ps_version', '1.7');
        } elseif (version_compare(@constant('_PS_VERSION_'), '1.6', '>=') && version_compare(@constant('_PS_VERSION_'), '1.7', '<')) {
            $this->context->smarty->assign('ps_version', '1.6');
        } else {
            $this->context->smarty->assign('ps_version', '1.5');
        }

        $this->context->smarty->assign('success', '');

        $carrier = new Carrier(Configuration::get('ORDERADMIN_CARRIER_ID'));

        if (Tools::isSubmit('form_submitted') && $this->validateSettings()) {
            $serverAddress = rtrim(Tools::getValue('orderadmin_server_address'), '/') . '/';
            Configuration::updateValue('ORDERADMIN_SERVER_ADDRESS', $serverAddress);
            Configuration::updateValue('ORDERADMIN_USERNAME', Tools::getValue('orderadmin_username'));
            Configuration::updateValue('ORDERADMIN_PASSWORD', Tools::getValue('orderadmin_password'));
            Configuration::updateValue('ORDERADMIN_OWNER', Tools::getValue('orderadmin_owner'));
            Configuration::updateValue('ORDERADMIN_SENDER', Tools::getValue('orderadmin_sender'));
            Configuration::updateValue('ORDERADMIN_DEFAULT_SHOP', Tools::getValue('orderadmin_default_shop'));
            Configuration::updateValue('ORDERADMIN_DEFAULT_SOURCE', Tools::getValue('orderadmin_default_source'));
            if (empty(Configuration::get('ORDERADMIN_SECRET'))) {
                $secret = md5(date('Y-m-d m:i:s'));
                Configuration::updateValue('ORDERADMIN_SECRET', $secret);
            }
//            $carrier->active = Tools::getValue('orderadmin_status') ? true : false;
            $carrier->update();

            $this->context->smarty->assign(
                'success',
                $this->l('Успешно променихте параметрите на Orderadmin!')
            );

        }

        if (isset($this->error_arr['error_warning'])) {
            $this->context->smarty->assign('error_warning', $this->error_arr['error_warning']);
        } else {
            $this->context->smarty->assign('error_warning', '');
        }

        if (isset($this->error_arr['error_username'])) {
            $this->context->smarty->assign('error_username', $this->error_arr['error_username']);
        } else {
            $this->context->smarty->assign('error_username', '');
        }

        if (isset($this->error_arr['error_password'])) {
            $this->context->smarty->assign('error_password', $this->error_arr['error_password']);
        } else {
            $this->context->smarty->assign('error_password', '');
        }

        if (isset($this->error_arr['error_shop'])) {
            $this->context->smarty->assign('error_shop', $this->error_arr['error_shop']);
        } else {
            $this->context->smarty->assign('error_shop', '');
        }

        if (isset($this->error_arr['error_source'])) {
            $this->context->smarty->assign('error_source', $this->error_arr['error_source']);
        } else {
            $this->context->smarty->assign('error_source', '');
        }

        if (isset($this->error_arr['error_owner'])) {
            $this->context->smarty->assign('error_owner', $this->error_arr['error_owner']);
        } else {
            $this->context->smarty->assign('error_owner', '');
        }

        if (isset($this->error_arr['error_sender'])) {
            $this->context->smarty->assign('error_sender', $this->error_arr['error_sender']);
        } else {
            $this->context->smarty->assign('error_sender', '');
        }

        if (isset($this->error_arr['error_server'])) {
            $this->context->smarty->assign('error_server', $this->error_arr['error_server']);
        } else {
            $this->context->smarty->assign('error_server', '');
        }

        $this->context->smarty->assign('orderadmin_version', $this->version);

        if (Tools::getIsset('orderadmin_server_address') && Tools::getValue('orderadmin_server_address')) {
            $this->context->smarty->assign('orderadmin_server_address', Tools::getValue('orderadmin_server_address'));
        } else {
            $this->context->smarty->assign('orderadmin_server_address', Configuration::get('ORDERADMIN_SERVER_ADDRESS'));
        }

        if (Tools::getValue('orderadmin_username')) {
            $this->context->smarty->assign('orderadmin_username', Tools::getValue('orderadmin_username'));
        } else {
            $this->context->smarty->assign('orderadmin_username', Configuration::get('ORDERADMIN_USERNAME'));
        }

        if (Tools::getValue('orderadmin_password')) {
            $this->context->smarty->assign('orderadmin_password', Tools::getValue('orderadmin_password'));
        } else {
            $this->context->smarty->assign('orderadmin_password', Configuration::get('ORDERADMIN_PASSWORD'));
        }

        if (Tools::getValue('orderadmin_owner')) {
            $this->context->smarty->assign('orderadmin_owner', Tools::getValue('orderadmin_owner'));
        } else {
            $this->context->smarty->assign('orderadmin_owner', Configuration::get('ORDERADMIN_OWNER'));
        }

        if (Tools::getValue('orderadmin_sender')) {
            $this->context->smarty->assign('orderadmin_sender', Tools::getValue('orderadmin_sender'));
        } else {
            $this->context->smarty->assign('orderadmin_sender', Configuration::get('ORDERADMIN_SENDER'));
        }

        if (Tools::getValue('orderadmin_default_shop')) {
            $this->context->smarty->assign('orderadmin_default_shop', Tools::getValue('orderadmin_default_shop'));
        } else {
            $this->context->smarty->assign('orderadmin_default_shop', Configuration::get('ORDERADMIN_DEFAULT_SHOP'));
        }

        if (Tools::getValue('orderadmin_default_source')) {
            $this->context->smarty->assign('orderadmin_default_source', Tools::getValue('orderadmin_default_source'));
        } else {
            $this->context->smarty->assign('orderadmin_default_source', Configuration::get('ORDERADMIN_DEFAULT_SOURCE'));
        }

        if (Tools::getValue('orderadmin_secret')) {
            $this->context->smarty->assign('orderadmin_secret', Tools::getValue('orderadmin_secret'));
        } else {
            $this->context->smarty->assign('orderadmin_secret', Configuration::get('ORDERADMIN_SECRET'));
        }

        if ((int)Configuration::get('ORDERADMIN_HOOK_ORDER') !== 1) {
            $this->context->smarty->assign('orderadmin_hook_order', 0);
        } else {
            $this->context->smarty->assign('orderadmin_hook_order', 1);
        }

        if ((int)Configuration::get('ORDERADMIN_HOOK_ORDER_EDIT') !== 1) {
            $this->context->smarty->assign('orderadmin_hook_order_edit', 0);
        } else {
            $this->context->smarty->assign('orderadmin_hook_order_edit', 1);
        }

        if ((int)Configuration::get('ORDERADMIN_HOOK_ORDER_STATUS') !== 1) {
            $this->context->smarty->assign('orderadmin_hook_order_status', 0);
        } else {
            $this->context->smarty->assign('orderadmin_hook_order_status', 1);
        }

        if ((int)Configuration::get('ORDERADMIN_HOOK_PRODUCT') !== 1) {
            $this->context->smarty->assign('orderadmin_hook_product', 0);
        } else {
            $this->context->smarty->assign('orderadmin_hook_product', 1);
        }

        if ((int)Configuration::get('ORDERADMIN_HOOK_ADDRESS') !== 1) {
            $this->context->smarty->assign('orderadmin_hook_address', 0);
        } else {
            $this->context->smarty->assign('orderadmin_hook_address', 1);
        }

        // Check requirments
        $php_version = preg_replace('/^([0-9\.]+).*/', '$1', phpversion());

        $mysql_version = Db::getInstance()->getRow("SELECT VERSION() as mysql_version");
        $mysql_version = preg_replace('/^([0-9\.]+).*/', '$1', $mysql_version['mysql_version']);

        $requirements = array(
            array(
                'name' => $this->l('Минимална версия на PHP'),
                'required' => '5.6',
                'current' => $php_version,
                'is_success' => (version_compare($php_version, '5.6', '>='))
            ),
            array(
                'name' => $this->l('Минимална версия на MySQL'),
                'required' => '5.0.x',
                'current' => $mysql_version,
                'is_success' => (version_compare($mysql_version, '5', '>='))
            ),
        );
        $this->context->smarty->assign('requirements', $requirements);

        $this->context->smarty->assign('action', Tools::safeOutput($_SERVER['REQUEST_URI']));

        return $this->display(__FILE__, 'orderadmin_form.tpl');
    }

    //validate admin panel module settings
    private function validateSettings() {

        $error = false;

        if (!Tools::getValue('orderadmin_server_address')) {
            $this->context->smarty->assign(
                'error',
                $this->l('Моля, въведете URL!')
            );
            $error = true;
        }

        if (!Tools::getValue('orderadmin_username')) {
            $this->context->smarty->assign(
                'error',
                $this->l('Моля, въведете потребителско име!')
            );
            $error = true;
        }

        if (!Tools::getValue('orderadmin_password')) {
            $this->context->smarty->assign(
                'error',
                $this->l('Моля, въведете парола!')
            );
            $error = true;
        }

        if (!Tools::getValue('orderadmin_owner')) {
            $this->context->smarty->assign(
                'error',
                $this->l('Моля, ID на притежателя на магазина!')
            );
            $error = true;
        }

        if (!Tools::getValue('orderadmin_default_shop')) {
            $this->context->smarty->assign(
                'error',
                $this->l('Моля, въведете ID на магазина в Orderadmin!')
            );
            $error = true;
        }

        if (!$error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //import data from OA
    public  function  importOrderadminData() {

        $errorMessages = [];

        $ownerId = (int)Configuration::get('ORDERADMIN_OWNER');

        $path = 'api/integrations/sources';
        $orderMonitorStates = $this->getData(
            $path,
            'source',
            [
                'filter' => [
                    [
                        'field' => 'state',
                        'type' => 'eq',
                        'value' => 'active'
                    ],
                    [
                        'field' => 'owner',
                        'type' => 'eq',
                        'value' => $ownerId
                    ],
                    [
                        'field' => 'handler',
                        'type' => 'eq',
                        'value' => 'prestashop'
                    ]
                ]
            ]
        );

        $states = $orderMonitorStates[0]['settings']['Orders']['monitor']['states'];

        if (!empty($states)) {
            $delQuery = sprintf(
                'TRUNCATE TABLE %sorderadmin_orders_monitor',
                _DB_PREFIX_
            );
            Db::getInstance()->Execute($delQuery);

            foreach ($states as $key => $value) {
                if ((int)$value !== 0) {
                    $sqlQuery = sprintf(
                        'INSERT INTO %sorderadmin_orders_monitor(`state_id`, `orderadmin_state`) VALUES(%s,"%s") ON DUPLICATE KEY UPDATE orderadmin_state = "%s"',
                        _DB_PREFIX_,
                        (int)$value,
                        $key,
                        $key
                    );

                    if(!Db::getInstance()->Execute($sqlQuery)) {
                        $errorMessages[] = 'Orders monitor states not imported';
                    }
                }
            }
        } else {
            $errorMessages[] = 'Orders monitor states not imported';
        }

        $exportStates = $orderMonitorStates[0]['settings']['Orders']['importStates'];

        if (!empty($exportStates)) {
            $delQuery = sprintf(
                'TRUNCATE TABLE %sorderadmin_export_states',
                _DB_PREFIX_
            );
            Db::getInstance()->Execute($delQuery);

            foreach ($exportStates as $key => $value) {
                if ($value === true) {
                    $sqlQuery = sprintf(
                        'INSERT INTO %sorderadmin_export_states(`state_id`) VALUES(%s)',
                        _DB_PREFIX_,
                        (int)$key
                    );

                    if(!Db::getInstance()->Execute($sqlQuery)) {
                        $errorMessages[] = 'Orders monitor states not imported';
                    }
                }
            }
        }

        $data = [
            'success' => 'Успешно импортнахте данни от Orderadmin!',
            'errors' => $errorMessages
        ];

        echo Tools::jsonEncode($data);
    }

    //get data from OA
    public  function getData($path, $identifier, array $query, $i = 1)
    {
        $username = Configuration::get('ORDERADMIN_USERNAME');
        $password = Configuration::get('ORDERADMIN_PASSWORD');
        $server = Configuration::get('ORDERADMIN_SERVER_ADDRESS');

        $url = $server.$path;

        if (!empty($query)) {
            $url = $url . '?' . http_build_query($query) . '&page=' . $i;
        } else {
            $url = $url . '&page=' . $i;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Accept: application/json',
            ]
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, self::TYPE_GET);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);

        $this->result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $result = $this->getResult();
        $return = null;
        $sqlQuery = '';

        if ($httpCode === 200) {

            switch ($identifier) {
                case 'product_offer':
                    $productOffer = $result['_embedded'][$identifier][0];

                    if(!empty($productOffer['id'])) {
                        $sqlQuery = sprintf(
                            'INSERT INTO %sorderadmin_products(`product_id`, `product_extId`, `oa_product_type`) VALUES("%s", "%s", "%s") ON DUPLICATE KEY UPDATE product_status = "Зареден в Orderadmin", oa_product_type = "%s", product_extId = "%s"',
                            _DB_PREFIX_,
                            trim($query['filter'][1]['value']),
                            trim($productOffer['id']),
                            trim($productOffer['type']),
                            trim($productOffer['type']),
                            trim($productOffer['id'])
                        );

                        $return = $productOffer;
                    }
                    break;

                case 'order':
                    $order = $result['_embedded'][$identifier][0];

                    if(!empty($order['id'])) {

                        $sqlQuery = sprintf('INSERT INTO %sorderadmin_orders(`order_id`, `order_extId`%s) VALUES(%s, %s%s) ON DUPLICATE KEY UPDATE order_status = "Заредена в Orderadmin", order_extId = %s',
                            _DB_PREFIX_,
                            !empty($order['eav']['delivery-services-request']) ? ', `dr_id`' : '',
                            (int)($query['filter'][1]['value']),
                            (int)($order['id']),
                            !empty($order['eav']['delivery-services-request']) ? ', ' .(int)$order['eav']['delivery-services-request'] : $order['eav']['delivery-services-request'],
                            (int)($order['id'])
                        );

                        $return = $order;
                    }
                    break;

                default:
                    $return = $result['_embedded'][$identifier];
                    break;
            }

        }  else {
            $return = $httpCode;
        }

        if (!empty($sqlQuery)) {
            Db::getInstance()->Execute($sqlQuery);
        }

        return $return;
    }

    //post data to OA
    public function postToOrderadmin($data, $api, $type, $encode = true, $itemId = 0) {
        $username = Configuration::get('ORDERADMIN_USERNAME');
        $password = Configuration::get('ORDERADMIN_PASSWORD');
        $server = Configuration::get('ORDERADMIN_SERVER_ADDRESS');

        $url = $server . $api;

        if($encode === true) {
            $jquery_data = json_encode($data);
        } else {
            $jquery_data = $data;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Accept: application/json',
            ]
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jquery_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);

        $this->result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $returnedData = $this->getResult();
        $return = null;
        $sqlQuery = '';

        if ($type == self::TYPE_DELETE) {
            if ($httpCode === 404) {
                $sqlQuery = sprintf("UPDATE %sorderadmin_order_products_awaiting SET `execution_time` = now(), `result` = '%s' WHERE `orderadmin_order_products_awaiting_id` = %s",
                    _DB_PREFIX_,
                    $returnedData['detail'],
                    (int)$itemId
                );
            }
            Db::getInstance()->Execute($sqlQuery);

            return;
        }

        if ($httpCode === 201)
        {
            switch ($api) {
                case 'api/products/offer':
                    $productExtId = $returnedData['id'];

                    $sqlQuery = sprintf('INSERT INTO %sorderadmin_products(`product_id`, `product_extId`, `oa_product_type`) VALUES("%s","%s","%s")',
                        _DB_PREFIX_,
                        trim($data['extId']),
                        trim($productExtId),
                        trim($returnedData['type'])
                    );
                    $return = $returnedData;
                    break;

                case 'api/products/order':
                    $orderExtId = $returnedData['id'];

                    $sqlQuery = sprintf('INSERT INTO %sorderadmin_orders(`order_id`, `order_extId`) VALUES(%s,%s) ON DUPLICATE KEY UPDATE order_status = "Заредена в Orderadmin", order_extId = %s',
                        _DB_PREFIX_,
                        $data['extId'],
                        $orderExtId,
                        $orderExtId
                    );
                    break;

                case 'api/delivery-services/requests':
                    $drId = $returnedData['id'];
                    $orderExtId = $returnedData['extId'];

                    $sqlQuery = sprintf('UPDATE %sorderadmin_orders SET dr_id = %s WHERE order_extId = %s',
                        _DB_PREFIX_,
                        (int)$drId,
                        (int)$orderExtId
                    );
                    $return = $drId;
                    break;

                case 'api/products/order/product':

                    $sqlQuery = sprintf("UPDATE %sorderadmin_order_products_awaiting SET `execution_time` = now(), `result` = '%s' WHERE `orderadmin_order_products_awaiting_id` = %s",
                        _DB_PREFIX_,
                        json_encode($returnedData),
                        (int)$itemId
                    );
                    break;
            }

        } elseif ($httpCode === 204) {

            $sqlQuery = sprintf("UPDATE %sorderadmin_order_products_awaiting SET `execution_time` = now(), `result` = '%s' WHERE `orderadmin_order_products_awaiting_id` = %s",
                _DB_PREFIX_,
                $httpCode,
                (int)$itemId
            );

        } else if ($httpCode === 200 && $encode == false) {

            $sqlQuery = sprintf("UPDATE %sorderadmin_order_products_awaiting SET `execution_time` = now(), `result` = '%s' WHERE `orderadmin_order_products_awaiting_id` = %s",
                _DB_PREFIX_,
                json_encode($returnedData),
                (int)$itemId
            );

        } else if (!in_array($httpCode, [201,200])) {

            switch ($api) {
                case 'api/products/order':
                    $sqlQuery = sprintf('INSERT INTO %sorderadmin_orders(`order_id`, `order_extId`, `order_status`) VALUES(%s, null, "Code %s") ON DUPLICATE KEY UPDATE order_status = "Code %s"',
                        _DB_PREFIX_,
                        $data['extId'],
                        $httpCode .' '. substr($returnedData['detail'],0, 55),
                        $httpCode .' '. substr($returnedData['detail'],0, 55)
                    );
                    break;

                case 'api/products/offer':
                    $sqlQuery = sprintf(
                        'INSERT INTO %sorderadmin_products(`product_id`, `product_extId`, `oa_product_type`, `product_status`) VALUES("%s", null, null, "%s") ON DUPLICATE KEY UPDATE product_status = "%s"',
                        _DB_PREFIX_,
                        trim($data['extId']),
                        $httpCode .' '. substr($returnedData['detail'],0, 55),
                        $httpCode .' '. substr($returnedData['detail'],0, 55)
                    );
                    break;
            }

        }

        if (!empty($sqlQuery)) {
            Db::getInstance()->Execute($sqlQuery);
        }

        return $return;
    }

    //formatting received data from getData()
    public function getResult($format = true)
    {
        if ($format) {
            return json_decode($this->result, true);
        } else {
            return $this->result;
        }
    }

    //Order save action
    public function hookActionValidateOrder($params)
    {
        $exportStatesQ = sprintf(
            'SELECT state_id FROM %sorderadmin_export_states',
            _DB_PREFIX_
        );

        $exportStates = Db::getInstance()->ExecuteS($exportStatesQ);
        if (!in_array($params['order']->current_state, array_column($exportStates,'state_id'))) {
            return;
        }

        $shopId = Configuration::get('ORDERADMIN_DEFAULT_SHOP');
        $sourceId = Configuration::get('ORDERADMIN_DEFAULT_SOURCE');
        $address = new Address(intval($params['order']->id_address_delivery));

        $api = 'api/products/order';

        $offers = [];

        foreach ($params['order']->getProducts() as $product) {

            if (! empty($product['product_attribute_id'])) {
                $extId = sprintf(
                    '%s-%s',
                    $product['product_id'],
                    $product['product_attribute_id']
                );
            } else {
                $extId = sprintf(
                    '%s',
                    $product['product_id']
                );
            }

            $offers[] = [
                'productOffer' => [
                    'shop'      => $shopId,
                    'extId'     => $extId,
                    'name'      => $product['product_name'],
                    'type'      => 'simple',
                    'barcodes'  => $product['ean13'],
                ],
                'count'         => (int)$product['product_quantity'],
                'price'         => '' . $product['unit_price_tax_incl'] . '',
                'tax'           => null,
                'discountPrice' => null,
                'total'         => '' . $product['total_price_tax_incl'] . '',
            ];
        }

        $postData = [
            'profile' => [
                'name'       => $params['customer']->firstname . ' ' . $params['customer']->lastname,
                'phone' => !empty(trim($address->phone)) ? $address->phone : (!empty(trim($address->phone_mobile)) ? $address->phone_mobile : null),
                'phones' => [
                    [
                        'phone' => !empty(trim($address->phone)) ? $address->phone : (!empty(trim($address->phone_mobile)) ? $address->phone_mobile : null),
                    ],
                ],
                'email'      => $params['customer']->email,
                'extId'      => $params['customer']->id,
                //                'country'    => $address->country,
            ],
            'address' => [
                'street'     => $address->address1 . ', ' . $address->address2,
                'notFormal'  => $address->address1 . ', ' . $address->address2,
                'postcode'   => $address->postcode,
                'country'    => $address->country,
                'extId'      => $params['order']->id,
            ],
            'shop'           => $shopId,
            'noTask'         => 'true',
            'extId'          => $params['order']->id,
            'date'           => $params['order']->date_add,
            'paymentState'   => 'not_paid',
            'state'          => 'pending_queued',
            'orderPrice'     => '' . $params['order']->total_products_wt . '',
            'totalPrice'     => '' . $params['order']->total_paid . '',
            'recipientName'  => $params['customer']->firstname . ' ' . $params['customer']->lastname,
            'email'  => $params['customer']->email,
            'source' => (int)$sourceId,
            'orderProducts'  => $offers,
        ];

        if ($params['order']->payment != 'Наложен платеж (НП)') {
            $postData['paymentState'] = 'paid';
        }

        $this->postToOrderadmin($postData, $api, self::TYPE_POST);
    }

    //Order status change Admin
    public function hookActionOrderStatusPostUpdate($params) {
        $sqlState = sprintf(
            'SELECT orderadmin_state FROM %sorderadmin_orders_monitor WHERE state_id = %s',
            _DB_PREFIX_,
            (int)$params['newOrderStatus']->id
        );

        $orderStatus = Db::getInstance()->getValue($sqlState);

        if (!empty($orderStatus)) {
            $sqlOrder = sprintf(
                'SELECT order_extId FROM %sorderadmin_orders WHERE order_id = %s',
                _DB_PREFIX_,
                (int)$params['id_order']
            );

            $orderId = Db::getInstance()->getValue($sqlOrder);

            if (!empty($orderId)) {
                $api = 'api/products/order/' . (int)$orderId;
                $postData = [
                    'state'  => $orderStatus,
                    'noTask' => 'true',
                ];
                $this->postToOrderadmin($postData, $api, self::TYPE_PATCH);
            } else {
                $shopId = Configuration::get('ORDERADMIN_DEFAULT_SHOP');

                $oaOrderData = $this->getData(
                    'api/products/order',
                    'order',
                    [
                        'filter' => [
                            [
                                'field' => 'shop',
                                'type'  => 'eq',
                                'value' => $shopId
                            ],
                            [
                                'field' => 'extId',
                                'type'  => 'eq',
                                'value' => $params['id_order']
                            ]
                        ]
                    ]
                );

                if (!empty($oaOrderData['id'])) {
                    $api = 'api/products/order/' . (int)$oaOrderData['id'];
                    $postData = [
                        'state'  => $orderStatus,
                        'noTask' => 'true',
                    ];
                    $this->postToOrderadmin(
                        $postData, $api, self::TYPE_PATCH
                    );
                } else {
                    $order = new Order((int)$params['id_order']);
                    $customer = new Customer((int)$order->id_customer);

                    $par = [
                        'order'    => $order,
                        'customer' => $customer,
                        //                            'newStatus' => (int)$params['newOrderStatus']->id
                    ];

                    $this->hookActionValidateOrder($par);
                }
            }
        }
        return true;
    }

    //Order Admin Edit
    public function hookActionOrderEdited ($params) {
        $sql = sprintf(
            'SELECT order_extId FROM %sorderadmin_orders WHERE order_id = %s',
            _DB_PREFIX_,
            (int)$params['order']->id
        );

        $orderExtId= Db::getInstance()->getValue($sql);
        $shopId = Configuration::get('ORDERADMIN_DEFAULT_SHOP');

        if (empty($orderExtId)) {
            $oaOrderData = $this->getData(
                'api/products/order',
                'order',
                [
                    'filter' => [
                        [
                            'field' => 'shop',
                            'type' => 'eq',
                            'value' => $shopId
                        ],
                        [
                            'field' => 'extId',
                            'type' => 'eq',
                            'value' => $params['order']->id
                        ]
                    ]
                ]
            );

            if (empty($oaOrderData)) {
                $customer = new Customer((int)$params['order']->id_customer);
                $par = [
                    'order' => $params['order'],
                    'customer' =>  $customer
                ];

                $this->hookActionValidateOrder($par);
            } else {
                $orderExtId = $oaOrderData['id'];
                $products_list = $params['order']->getProducts();

                $this->editOrderProducts($shopId, $orderExtId, $products_list);
            }
        } else {
            $products_list = $params['order']->getProducts();

            $this->editOrderProducts($shopId, $orderExtId, $products_list);
        }

        $orderData = [
            'orderPrice'     => number_format(floatval($params['order']->total_products_wt),2, '.', ''),
            'totalPrice'     => number_format(floatval($params['order']->total_paid),2, '.', '')
        ];
        $this->postToOrderadmin(
            $orderData,
            'api/products/order/' . (int)$orderExtId,
            self::TYPE_PATCH
        );

        return null;
    }

    //Product save
    public function hookActionProductSave ($params) {
        $product = $params['product'];

        if ($product->date_add === $product->date_upd)
            return null;

        $shopId = Configuration::get('ORDERADMIN_DEFAULT_SHOP');
        $langId = $this->context->language->id;
        $combinations = $product->getAttributesResume($langId);
        $link = new Link;
        $img = $product->getCover($product->id);
        $img_url = $link->getImageLink(isset($product->link_rewrite) ? $product->link_rewrite[1] : $product->name[1], (int)$img['id_image'], 'medium_default');
        $protocol = strpos(
            strtolower(
                $_SERVER['SERVER_PROTOCOL']
            ),
            'https'
        ) === FALSE ? 'http' : 'https';
        $domainLink = $protocol . '://' . $_SERVER['HTTP_HOST'];

        //offer data to post to OA
        $offerData = [
            'state' => 'normal',
            'extId' => $product->id,
            'image' => $domainLink . $img_url,
            'weight' => ! empty($product->weight)
                ? round($product->weight, 2) : null,
            'sku' => ! empty($product->reference)
                ? $product->reference : null,
            'article' => ! empty($product->reference)
                ? $product->reference : null,
            'name' => ! empty($product->name[1]) ? $product->name[1] : $product->name[2],
            'price' => round(
                $product->price, 2),
            'purchasingPrice' => round(
                $product->wholesale_price, 2),
            'noTask' => 'true',
            'barcodes' => [
                $product->ean13
            ]
        ];

        //check if offer exists in ps_orderadmin_products table
        $sql = sprintf(
            'SELECT * FROM %sorderadmin_products WHERE product_id = "%s"',
            _DB_PREFIX_,
            trim($product->id)
        );

        $oaProduct = Db::getInstance()->ExecuteS($sql);

        // if we dont find extId in the result we try to get the offer from OA
        if (empty($oaProduct[0]['product_extId'])) {

            $oaData = $this->getData(
                'api/products/offer',
                'product_offer',
                [
                    'filter' => [
                        [
                            'field' => 'shop',
                            'type' => 'eq',
                            'value' => $shopId
                        ],
                        [
                            'field' => 'extId',
                            'type' => 'eq',
                            'value' => strval(trim($product->id))
                        ],
                    ]
                ]
            );

            //if result insert it to ps_orderadmin_products table in the getData()
            if (!empty($oaData['id'])) {
                //if result offer is simple type and PS product
                //has combinations PATCH the OA offer with new type
                //configurable and POST each combination

                if ($oaData['type'] === 'simple' && !empty($combinations)) {
                    $offerData['type'] = 'configurable';
                    $this->postToOrderadmin(
                        $offerData,
                        'api/products/offer/' . (int)$oaData['id'],
                        self::TYPE_PATCH
                    );

                    foreach ($combinations as $combination) {
                        $sqlQuery = sprintf(
                            'INSERT INTO %sorderadmin_awaiting(`attr_id`, `product_id`) VALUES(%s, %s) ON DUPLICATE KEY UPDATE product_id = %s',
                            _DB_PREFIX_,
                            (int)$combination['id_product_attribute'],
                            (int)$combination['id_product'],
                            (int)$combination['id_product']
                        );
                        Db::getInstance()->Execute($sqlQuery);
                    }

                    //if result offer is configurable type and PS product
                    //has combinations PATCH the OA offer and check if
                    //each combination exists in ps_orderadmin_products table
                    //if combination exists with extId we do PATCH to OA offer
                    //if combination doesn't exist we try to get if from OA
                    //if we get offer from OA we insert it to ps_orderadmin_products table and PATCH OA offer
                    //if we dont get offer from OA we POST the combination offer to OA
                } elseif ($oaData['type'] === 'configurable' && !empty($combinations)) {
                    $this->postToOrderadmin(
                        $offerData,
                        'api/products/offer/' . (int)$oaData['id'],
                        self::TYPE_PATCH
                    );

                    foreach ($combinations as $combination) {
                        $sqlQuery = sprintf(
                            'INSERT INTO %sorderadmin_awaiting(`attr_id`, `product_id`) VALUES(%s, %s) ON DUPLICATE KEY UPDATE product_id = %s',
                            _DB_PREFIX_,
                            (int)$combination['id_product_attribute'],
                            (int)$combination['id_product'],
                            (int)$combination['id_product']
                        );
                        Db::getInstance()->Execute($sqlQuery);
                    }

                    //if result offer is simple type and PS product
                    //has no combinations PATCH the OA offer
                } elseif($oaData['type'] === 'simple' && empty($combinations)) {
                    $this->postToOrderadmin(
                        $offerData,
                        'api/products/offer/' . (int)$oaData['id'],
                        self::TYPE_PATCH
                    );
                }

            } else {
                $offerData['shop'] = $shopId;

                if (empty($combinations)) {

                    $offerData['type'] = 'simple';

                    $this->postToOrderadmin(
                        $offerData,
                        'api/products/offer',
                        self::TYPE_POST
                    );
                } else {
                    $offerData['type'] = 'configurable';

                    $this->postToOrderadmin(
                        $offerData,
                        'api/products/offer',
                        self::TYPE_POST
                    );

                    foreach ($combinations as $combination) {
                        $sqlQuery = sprintf(
                            'INSERT INTO %sorderadmin_awaiting(`attr_id`, `product_id`) VALUES(%s, %s) ON DUPLICATE KEY UPDATE product_id = %s',
                            _DB_PREFIX_,
                            (int)$combination['id_product_attribute'],
                            (int)$combination['id_product'],
                            (int)$combination['id_product']
                        );
                        Db::getInstance()->Execute($sqlQuery);
                    }
                }
            }
        } else {
            $oaProductId = $oaProduct[0]['product_extId'];

            if (empty($combinations)) {
                $this->postToOrderadmin(
                    $offerData,
                    'api/products/offer/' . (int)$oaProductId,
                    self::TYPE_PATCH
                );
            } else {
                if($oaProduct[0]['oa_product_type'] === 'simple') {
                    $offerData['type'] = 'configurable';
                    $updSql = sprintf(
                        'UPDATE %sorderadmin_products SET oa_product_type = "configurable" WHERE product_id = "%s"',
                        _DB_PREFIX_,
                        trim($oaProduct[0]['product_id'])
                    );

                    Db::getInstance()->Execute($updSql);
                }

                $this->postToOrderadmin(
                    $offerData,
                    'api/products/offer/' . (int)$oaProductId,
                    self::TYPE_PATCH
                );

                foreach ($combinations as $combination) {
                    $sqlQuery = sprintf(
                        'INSERT INTO %sorderadmin_awaiting(`attr_id`, `product_id`) VALUES(%s, %s) ON DUPLICATE KEY UPDATE product_id = %s',
                        _DB_PREFIX_,
                        (int)$combination['id_product_attribute'],
                        (int)$combination['id_product'],
                        (int)$combination['id_product']
                    );
                    Db::getInstance()->Execute($sqlQuery);
                }
            }
        }
    }

    //Address Admin Edit
    public function hookActionAdminAddressSave($params) {
        $ownerId = (int)Configuration::get('ORDERADMIN_OWNER');

        $oaAddress = $this->getData(
            'api/clients/address',
            'address',
            [
                'filter' => [
                    [
                        'field' => 'owner',
                        'type' => 'eq',
                        'value' => $ownerId
                    ],
                    [
                        'field' => 'extId',
                        'type' => 'eq',
                        'value' => $params['id_order']
                    ]
                ]
            ]
        );

        if (!empty($oaAddress[0])) {
            $this->postToOrderadmin(
                [
                    'notFormal'  => $params[0]->address1 . ', ' . $params[0]->address2,
                    'street'     => $params[0]->address1 . ', ' . $params[0]->address2,
                    'postcode'   => $params[0]->postcode,
                ],
                'api/clients/address/'.$oaAddress[0]['id'],
                self::TYPE_PATCH
            );
        }

        return null;
    }

    //Order PATCH products in OA
    public function editOrderProducts($shopId, $orderExtId, array $products_list) {

        $oaOrderProducts = $this->getData(
            'api/products/order/product',
            'order_product',
            [
                'filter' => [
                    [
                        'field' => 'order',
                        'type' => 'eq',
                        'value' => $orderExtId
                    ],
                ]
            ]
        );

        if (count($oaOrderProducts) > count($products_list)) {
            foreach ($products_list as $pKey => $product) {
                if (!empty(intval($product['product_attribute_id']))) {
                    $product['extId'] = sprintf(
                        '%s-%s',
                        $product['product_id'],
                        $product['product_attribute_id']
                    );
                } else {
                    $product['extId'] = $product['product_id'];
                }

                foreach ($oaOrderProducts as $key => $value){
                    if ($product['extId'] == $value['productOfferRaw']['extId']) {
                        unset($products_list[$pKey]);
                        unset($oaOrderProducts[$key]);
                        break;
                    }
                }
            }

            if(!empty($oaOrderProducts)) {
                foreach ($oaOrderProducts as $p) {

                    $data = [
                        'id' => (int)$p['id']
                    ];

                    $jsonData = json_encode($data);

                    $sql = sprintf(
                        "INSERT INTO %sorderadmin_order_products_awaiting(`type`,`data`) VALUES('%s','%s');",
                        _DB_PREFIX_,
                        self::TYPE_DELETE,
                        $jsonData
                    );

                    Db::getInstance()->Execute($sql);
                }
            }

        } elseif (count($oaOrderProducts) < count($products_list)) {

            foreach ($products_list as $pKey => $product) {

                if (!empty(intval($product['product_attribute_id']))) {
                    $product['extId'] = sprintf(
                        '%s-%s',
                        $product['product_id'],
                        $product['product_attribute_id']
                    );
                } else {
                    $product['extId'] = $product['product_id'];
                }

                foreach ($oaOrderProducts as $key => $value){
                    if ($product['extId'] == $value['productOfferRaw']['extId']) {
                        unset($products_list[$pKey]);
                        unset($oaOrderProducts[$key]);
                        break;
                    }
                }
            }

            if (!empty($products_list)) {
                foreach ($products_list as $newProduct) {
                    $OfferData = [];
                    if (!empty(intval($newProduct['product_attribute_id']))) {
                        $productExtId = $product['extId'] = sprintf(
                            '%s-%s',
                            $newProduct['product_id'],
                            $newProduct['product_attribute_id']
                        );
                        $OfferData['parent'] = $newProduct['product_id'];
                    } else {
                        $productExtId = $newProduct['product_id'];
                        $OfferData['parent'] = null;
                    }

                    $sql = sprintf(
                        'SELECT product_extId FROM %sorderadmin_products WHERE product_id = "%s"',
                        _DB_PREFIX_,
                        $productExtId
                    );

                    $oaOfferId= Db::getInstance()->getValue($sql);

                    if (empty($oaOfferId)) {
                        $oaOfferData = $this->getData(
                            'api/products/offer',
                            'product_offer',
                            [
                                'filter' => [
                                    [
                                        'field' => 'shop',
                                        'type' => 'eq',
                                        'value' => $shopId
                                    ],
                                    [
                                        'field' => 'extId',
                                        'type' => 'eq',
                                        'value' => $productExtId
                                    ],
                                ]
                            ]
                        );

                        $oaOfferId = $oaOfferData['id'];
                    }

                    if (empty($oaOfferId)) {
                        $OfferData = [
                            'shop' => $shopId,
                            'extId' => $productExtId,
                            'state' => 'normal',
                            'type' => '	simple',
                            'weight' => ! empty($newProduct['weight'])
                                ? round($newProduct['weight'], 2) : null,
                            'sku' => ! empty($newProduct['reference'])
                                ? $newProduct['reference'] : null,
                            'article' => ! empty($newProduct['reference'])
                                ? $newProduct['reference'] : null,
                            'name' => $newProduct['product_name'],
                            'price' => round($newProduct['unit_price_tax_incl'], 2),
                            'purchasingPrice' => round($newProduct['unit_price_tax_incl'], 2),
                            'noTask' => 'true',
                            'barcodes' => [
                                $newProduct['ean13']
                            ]
                        ];

                        $oaOfferData = $this->postToOrderadmin(
                            $OfferData,
                            'api/products/offer',
                            self::TYPE_POST
                        );

                        $oaOfferId = $oaOfferData['id'];
                    }

                    $newOrderProduct = [
                        'order' => $orderExtId,
                        'productOffer' => [
                            'id' => $oaOfferId,
                            'shop' => $shopId
                        ],
                        'count'         => (int)$newProduct['product_quantity'],
                        'price'         => number_format(floatval($product['unit_price_tax_incl']),2,'.',''),
                        'tax'           => null,
                        'total'         => number_format(floatval($product['total_price_tax_incl']),2, '.', ''),
                    ];

                    $jsonData = json_encode($newOrderProduct);

                    $sql = sprintf(
                        "INSERT INTO %sorderadmin_order_products_awaiting(`type`,`data`) VALUES('%s','%s');",
                        _DB_PREFIX_,
                        self::TYPE_POST,
                        $jsonData
                    );

                    Db::getInstance()->Execute($sql);
                }
            }

        } else {
            foreach ($products_list as $pKey => $product) {

                if (!empty(intval($product['product_attribute_id']))) {
                    $product['extId'] = sprintf(
                        '%s-%s',
                        $product['product_id'],
                        $product['product_attribute_id']
                    );
                } else {
                    $product['extId'] = $product['product_id'];
                }

                foreach ($oaOrderProducts as $key => $value){
                    if(is_array($value) && key($value) === 'productOfferRaw') {
                        if ($product['extId'] === $value['productOfferRaw']['extId']) {
                            if((int)$product['product_quantity'] !== (int)$value['count']) {

                                $data = [
                                    'orderProductData' => [
                                        'count' => (int)$product['product_quantity'],
                                        'price' => number_format(floatval($product['unit_price_tax_incl']),2,'.',''),
                                        'total' => number_format(floatval($product['total_price_tax_incl']),2, '.', '')
                                    ],
                                    'id' => (int)$value['id']
                                ];


                                $jsonData = json_encode($data);

                                $sql = sprintf(
                                    "INSERT INTO %sorderadmin_order_products_awaiting(`type`,`data`) VALUES('%s','%s');",
                                    _DB_PREFIX_,
                                    self::TYPE_PATCH,
                                    $jsonData
                                );

                                Db::getInstance()->Execute($sql);
                            }
                        }
                    }
                }
            }
        }

        return;
    }

    //Admin Order View
    public function hookDisplayAdminOrderLeft($params)
    {
        $ordersToken = Tools::getAdminTokenLite('AdminOrders');
        $deliveryAddressInfo = null;
        $speedyOfficeId = null;
        $deliveryCity = null;
        $deliveryCityPostCode = null;
        $address = null;
        $loc_id = null;
        $trackingNumber = null;

        $id_lang = (int)$this->context->language->id;
        $order = new Order((int)$params['id_order'], false, $id_lang);

        $sql = sprintf(
            'SELECT * FROM %sorderadmin_orders WHERE order_id = %s',
            _DB_PREFIX_,
            (int)$params['id_order']
        );

        $result = Db::getInstance()->ExecuteS($sql);

        if(empty($result[0]['order_extId'])){
            $shopId = Configuration::get('ORDERADMIN_DEFAULT_SHOP');
            $oaOrderData = $this->getData(
                'api/products/order',
                'order',
                [
                    'filter' => [
                        [
                            'field' => 'shop',
                            'type' => 'eq',
                            'value' => $shopId
                        ],
                        [
                            'field' => 'extId',
                            'type' => 'eq',
                            'value' => $params['id_order']
                        ]
                    ]
                ]
            );

            if (empty($oaOrderData['id'])) {

                $customer = new Customer((int)$order->id_customer);

                $par = [
                    'order' => $order,
                    'customer' =>  $customer
                ];

                $this->hookActionValidateOrder($par);
            }

            $result = Db::getInstance()->ExecuteS($sql);
        }

        if (!empty($result[0]['order_extId']) && empty($result[0]['dr_id'])) {

            if (Configuration::get('SPEEDY_CARRIER_ID')) {
                $carrier = new Carrier(Configuration::get('SPEEDY_CARRIER_ID'));
                $carrier_active = $carrier->active;
            } else {
                $carrier_active = false;
            }

            if($carrier_active) {
                $sql = "SELECT * FROM " . _DB_PREFIX_ . "speedy_order WHERE order_id = '" . (int)$params['id_order'] . "'";

                $resultSql = Db::getInstance()->ExecuteS($sql);

                if (!empty($resultSql[0]['bol_id'])) {
                    $trackingNumber = $resultSql[0]['bol_id'];
                }

                if (!empty($resultSql[0]['data'])) {
                    $deliveryAddressInfo = unserialize($resultSql[0]['data']);
                }
            }

            if (empty($deliveryAddressInfo)) {
                $address = new Address(intval($order->id_address_delivery));
                $arr = explode('|',$address->other);
                foreach ($arr as $key => $val ) {
                    $a = explode(':', $val);
                    $deliveryAddressInfo[$a[0]] = $a[1];
                }
            }

            if (!empty($deliveryAddressInfo['office_id'])) {
                $speedyOfficeExtId = (int)$deliveryAddressInfo['office_id'];

                $oaServicePoint = $this->getData(
                    'api/delivery-services/service-points',
                    'servicePoints',
                    [
                        'filter' => [
                            [
                                'field' => 'deliveryService',
                                'type' => 'eq',
                                'value' => 23
                            ],
                            [
                                'field' => 'extId',
                                'type' => 'eq',
                                'value' => $speedyOfficeExtId
                            ]
                        ]
                    ]
                );

                if (!empty($oaServicePoint[0]['id'])) {
                    $speedyOfficeId = $oaServicePoint[0]['id'];

                    if (!empty($oaServicePoint[0]['rawAddress'])){
                        $addr =  sprintf(
                            '%s - %s',
                            $oaServicePoint[0]['name'],
                            $oaServicePoint[0]['rawAddress']
                        );
                    } else {
                        $addr =  sprintf(
                            '%s - %s',
                            $oaServicePoint[0]['name'],
                            $oaServicePoint[0]['raw']['address']['localAddressString']
                        );
                    }

                    $speedyOfficeAddress = $addr;

                    $loc_id = $oaServicePoint[0]['_embedded']['locality']['id'];
                    $deliveryCity = $oaServicePoint[0]['_embedded']['locality']['name'];
                    $deliveryCityPostCode = $oaServicePoint[0]['_embedded']['locality']['postcode'];

                }
            } else {
                if (!empty($deliveryAddressInfo['city']) && !empty($deliveryAddressInfo['postcode'])) {
                    $searchNeedles = ['/гр.\s/','/гр\s/','/гр./','/гр/','/Гр.\s/','/Гр./','/Гр/','/ГР.\s/','/ГР./','/ГР/','/ГР\s/'];
                    $deliveryCity = preg_replace($searchNeedles,'', $deliveryAddressInfo['city']);
                    $deliveryCityPostCode = $deliveryAddressInfo['postcode'];
                } else {
                    if (empty($address)) {
                        $id_lang = (int)$this->context->language->id;
                        $order = new Order((int)$params['id_order'], false, $id_lang);
                        $address = new Address(intval($order->id_address_delivery));
                    }

                    $cityString = $address->city;
                    $searchNeedles = ['/гр.\s/','/гр\s/','/гр./','/гр/','/Гр.\s/','/Гр./','/Гр/','/ГР.\s/','/ГР./','/ГР/','/ГР\s/'];
                    $deliveryCity = preg_replace($searchNeedles,'',$cityString);
                    $deliveryCityPostCode = $address->postcode;
                }

                $locality = $this->getData(
                    'api/locations/localities',
                    'localities',
                    [
                        'filter' => [
                            [
                                'field' => 'country',
                                'type' => 'eq',
                                'value' => 25
                            ],
                            [
                                'field' => 'name',
                                'type' => 'ilike',
                                'value' => $deliveryCity
                            ]
                        ]
                    ]
                );

                if(!empty($locality[0])) {
                    $loc_id = $locality[0]['id'];
                }
            }
        }

        if (version_compare(@constant('_PS_VERSION_'), '1.7', '>=')) {
            $this->context->smarty->assign('ps_version', '1.7');
        } elseif (version_compare(@constant('_PS_VERSION_'), '1.6', '>=') && version_compare(@constant('_PS_VERSION_'), '1.7', '<')) {
            $this->context->smarty->assign('ps_version', '1.6');
        } else {
            $this->context->smarty->assign('ps_version', '1.5');
        }

        $this->context->smarty->assign('this_path', $this->_path);

        $this->context->smarty->assign('order_id', $result[0]['order_id']);
        $this->context->smarty->assign('oa_order_id', $result[0]['order_extId']);
        $this->context->smarty->assign('dr_id', $result[0]['dr_id']);
        $this->context->smarty->assign('order_state', $result[0]['order_status']);
        $this->context->smarty->assign('ordersToken', $ordersToken);
        $this->context->smarty->assign('deliveryCity', $deliveryCity);
        $this->context->smarty->assign('deliveryCityPostCode', $deliveryCityPostCode);
        $this->context->smarty->assign('trackingNumber', $trackingNumber);
        if (!empty($speedyOfficeId)) {
            $this->context->smarty->assign('speedy_office_id', $speedyOfficeId);
        }
        if (!empty($speedyOfficeAddress)) {
            $this->context->smarty->assign('speedyOfficeAddress', $speedyOfficeAddress);
        }

        if (!empty($loc_id)) {
            $this->context->smarty->assign('loc_id', $loc_id);
        }

        return $this->display(__FILE__, 'orderadmin_order_info.tpl');
    }

    //Admin Product View
    public function hookDisplayAdminProductsExtra($params) {
        $sql = sprintf(
            'SELECT * FROM %sorderadmin_products WHERE product_id = "%s"',
            _DB_PREFIX_,
            $params['id_product']
        );

        $result = Db::getInstance()->ExecuteS($sql);

        if(empty($result[0]['product_extId'])){

            $id_lang = (int)$this->context->language->id;
            $product = new Product((int)$params['id_product'], false, $id_lang);

            if ($product->date_add === $product->date_upd && empty($product->name[1]) && empty($product->name[2]))
                return null;

            $shopId = Configuration::get('ORDERADMIN_DEFAULT_SHOP');

            $oaData = $this->getData(
                'api/products/offer',
                'product_offer',
                [
                    'filter' => [
                        [
                            'field' => 'shop',
                            'type' => 'eq',
                            'value' => $shopId
                        ],
                        [
                            'field' => 'extId',
                            'type' => 'eq',
                            'value' => trim($product->id)
                        ],
                    ]
                ]
            );

            //if result insert it to ps_orderadmin_products table in the getData()
            if (empty($oaData['id'])) {

                $combinations = $product->getAttributesResume($id_lang);
                $link = new Link;
                $img = $product->getCover($product->id);
                $img_url = $link->getImageLink(isset($product->link_rewrite) ? $product->link_rewrite[1] : $product->name[1], (int)$img['id_image'], 'medium_default');

                $protocol = strpos(
                    strtolower(
                        $_SERVER['SERVER_PROTOCOL']
                    ),
                    'https'
                ) === FALSE ? 'http' : 'https';
                $domainLink = $protocol . '://' . $_SERVER['HTTP_HOST'];

                //offer data to post to OA
                $offerData = [
                    'state' => 'normal',
                    'extId' => $product->id,
                    //                    'image' => _PS_BASE_URL_ . $img_url,
                    'image' => $domainLink . $img_url,
                    'shop'  => $shopId,
                    'weight' => ! empty($product->weight)
                        ? round($product->weight, 2) : null,
                    'sku' => ! empty($product->reference)
                        ? $product->reference : null,
                    'article' => ! empty($product->reference)
                        ? $product->reference : null,
                    'name' => $product->name,
                    'price' => round(
                        $product->getPrice(), 2),
                    'purchasingPrice' => round(
                        $product->price, 2),
                    'barcodes' => [
                        $product->ean13
                    ],
                    'noTask' => 'true',
                    'raw' => $product
                ];

                if (empty($combinations)) {
                    $offerData['type'] = 'simple';

                    $this->postToOrderadmin(
                        $offerData,
                        'api/products/offer',
                        self::TYPE_POST
                    );
                } else {
                    $offerData['type'] = 'configurable';

                    $this->postToOrderadmin(
                        $offerData,
                        'api/products/offer',
                        self::TYPE_POST
                    );

                    foreach ($combinations as $combination) {
                        $sqlQuery = sprintf(
                            'INSERT INTO %sorderadmin_awaiting(`attr_id`, `product_id`) VALUES(%s, %s) ON DUPLICATE KEY UPDATE product_id = %s',
                            _DB_PREFIX_,
                            (int)$combination['id_product_attribute'],
                            (int)$combination['id_product'],
                            (int)$combination['id_product']
                        );
                        Db::getInstance()->Execute($sqlQuery);
                    }
                }

            }
            $result = Db::getInstance()->ExecuteS($sql);
        }

        if (version_compare(@constant('_PS_VERSION_'), '1.7', '>=')) {
            $this->context->smarty->assign('ps_version', '1.7');
        } elseif (version_compare(@constant('_PS_VERSION_'), '1.6', '>=') && version_compare(@constant('_PS_VERSION_'), '1.7', '<')) {
            $this->context->smarty->assign('ps_version', '1.6');
        } else {
            $this->context->smarty->assign('ps_version', '1.5');
        }

        $this->context->smarty->assign('this_path', $this->_path);

        $this->context->smarty->assign('product_id', $result[0]['product_id']);
        $this->context->smarty->assign('oa_product_id', $result[0]['product_extId']);
        $this->context->smarty->assign('oa_product_type', $result[0]['oa_product_type']);
        $this->context->smarty->assign('product_state', $result[0]['product_status']);

        return $this->display(__FILE__, 'orderadmin_product_info.tpl');
    }

    //
    //CRON test methods
    //
    public function testComb(){
        $sql = sprintf(
            'SELECT attr_id AS attr, product_id AS product FROM %sorderadmin_awaiting LIMIT 1',
            _DB_PREFIX_
        );

        $dbRes = Db::getInstance()->ExecuteS($sql);

        if(!empty($dbRes[0]['attr']) && !empty($dbRes[0]['product'])) {

            $id_lang = (int)$this->context->language->id;

            $product = new Product((int)$dbRes[0]['product'], false, $id_lang);

            $att = $product->getAttributeCombinationsById((int)$dbRes[0]['attr'],$id_lang);

            $img = $product->getCombinationImageById((int)$dbRes[0]['attr'],$id_lang);

            $simpleOfferExtId = sprintf(
                '%s-%s',
                $att[0]['id_product'],
                $att[0]['id_product_attribute']
            );

            $simpleOfferData = [
                'image' => ! empty($img) ? $img : '',
                'weight' => ! empty($att['weight'])
                    ? round($att['weight'], 2) : round($product->weight, 2),
                'sku' => ! empty($att[0]['reference'])
                    ? $att[0]['reference'] : null,
                'article' => ! empty($att[0]['reference'])
                    ? $att[0]['reference'] : null,
                'price' => (intval($att[0]['price']) != 0) ? round(
                    $att[0]['price'] + $product->price, 2) : round($product->getPrice(), 2),
                'purchasingPrice' => (intval($att[0]['price']) != 0) ? round(
                    $att[0]['price'], 2) : round($product->price, 2),
                'noTask' => 'true',
                'barcodes' => [
                    $att[0]['ean13']
                ]
            ];

            $oaSimpSql = sprintf(
                'SELECT product_extId FROM %sorderadmin_products WHERE product_id = "%s"',
                _DB_PREFIX_,
                trim($simpleOfferExtId)
            );

            $oaSimpleProductExtid = Db::getInstance()->getValue($oaSimpSql);

            if (!empty($oaSimpleProductExtid)) {

                $this->postToOrderadmin(
                    $simpleOfferData,
                    'api/products/offer/' . (int)$oaSimpleProductExtid,
                    self::TYPE_PATCH
                );

            } else {
                $shopSql = sprintf(
                    'SELECT value FROM %sconfiguration WHERE name = "%s"',
                    _DB_PREFIX_,
                    trim('ORDERADMIN_DEFAULT_SHOP')
                );

                $shopId = Db::getInstance()->getValue($shopSql);

                $oaSimpleData = $this->getData(
                    'api/products/offer',
                    'product_offer',
                    [
                        'filter' => [
                            [
                                'field' => 'shop',
                                'type' => 'eq',
                                'value' => $shopId
                            ],
                            [
                                'field' => 'extId',
                                'type' => 'eq',
                                'value' => $simpleOfferExtId
                            ],
                        ]
                    ]
                );

                if (!empty($oaSimpleData['id'])) {

                    $this->postToOrderadmin(
                        $simpleOfferData,
                        'api/products/offer/' . (int)$oaSimpleData['id'],
                        self::TYPE_PATCH
                    );
                } else {
                    $parentSql = sprintf(
                        'SELECT product_extId FROM %sorderadmin_products WHERE product_id = "%s"',
                        _DB_PREFIX_,
                        $att[0]['id_product']
                    );

                    $parentId = Db::getInstance()->getValue($parentSql);

                    $simpleOfferData['shop'] = $shopId;
                    $simpleOfferData['extId'] = $simpleOfferExtId;
                    $simpleOfferData['type'] = 'simple';
                    $simpleOfferData['parent'] = (int)$parentId;
                    $simpleOfferData['state'] = 'normal';

                    if(count($att) > 1) {
                        $attName = [];
                        foreach ($att as $attribute)
                            $attName[] = $attribute['attribute_name'];
                        if (! count($attName))
                            return '-';
                        $simpleOfferData['name'] =  implode(' - ', $attName);
                    } else {
                        $simpleOfferData['name'] = $att[0]['attribute_name'];
                    }

                    $this->postToOrderadmin(
                        $simpleOfferData,
                        'api/products/offer',
                        self::TYPE_POST
                    );
                }
            }

            $delSql = sprintf(
                'DELETE FROM %sorderadmin_awaiting WHERE attr_id = %s',
                _DB_PREFIX_,
                (int)$dbRes[0]['attr']
            );

            Db::getInstance()->Execute($delSql);
        }
        return 'OK';
    }

    public function testOrder()
    {
        $sql = sprintf(
            'SELECT * FROM %sorderadmin_order_products_awaiting WHERE `execution_time` IS NULL AND `result` IS NULL LIMIT 1',
            _DB_PREFIX_
        );

        $dbRes = Db::getInstance()->ExecuteS($sql);

        $data = $dbRes[0];

        if(!empty($data)){
            switch ($data['type']) {
                case self::TYPE_POST:
                    $orderProductData = $data['data'];

                    $this->postToOrderadmin(
                        $orderProductData,
                        'api/products/order/product',
                        self::TYPE_POST,
                        false,
                        $data['orderadmin_order_products_awaiting_id']
                    );
                    break;

                case self::TYPE_PATCH:
                    $arr = (array)json_decode($data['data']);
                    $prodData = json_encode($arr['orderProductData']);

                    $this->postToOrderadmin(
                        $prodData,
                        'api/products/order/product/' . $arr['id'],
                        self::TYPE_PATCH,
                        false,
                        $data['orderadmin_order_products_awaiting_id']
                    );
                    break;

                case self::TYPE_DELETE:
                    $arr = (array)json_decode($data['data']);

                    $this->postToOrderadmin(
                        [],
                        'api/products/order/product/' . $arr['id'],
                        self::TYPE_DELETE,
                        false,
                        $data['orderadmin_order_products_awaiting_id']
                    );
                    break;
            }
        }
    }

}