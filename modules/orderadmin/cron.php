<?php

// instantiated to allow call of Legacy classes from classes in /src and /tests
require(dirname(__FILE__).'/../../config/config.inc.php');

switch ($argv[1]) {
    case 'OaOrders':
        require_once dirname(__FILE__) . '/cron/oaOrders.php';

        $oaOrders = new OaOrders();
        $oaOrders->initContent();
        break;

    case 'OaCombinations':
        require_once dirname(__FILE__) . '/cron/oaCombinations.php';

        $oaCombinations = new OaCombinations();
        $oaCombinations->initContent();
        break;

    case 'OaOffersUpdate':
        require_once dirname(__FILE__) . '/cron/oaOffersUpdate.php';

        $oaCombinations = new OaOffersUpdate();
        $oaCombinations->initContent();
        break;
}
