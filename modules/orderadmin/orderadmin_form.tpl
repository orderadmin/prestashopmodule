<br />

{if $success}
    {if ($ps_version == '1.6') OR ($ps_version == '1.7')}
        <div class="bootstrap">
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert" type="button">X</button>
                {$success}
            </div>
        </div>
    {else}
        <div class="conf">{$success}</div>
    {/if}
{/if}
{if $error_warning}
    {if ($ps_version == '1.6') OR ($ps_version == '1.7')}
        <div class="bootstrap">
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert" type="button">X</button>
                {$error_warning}
            </div>
        </div>
    {else}
        <div class="error">{$error_warning}</div>
    {/if}
{/if}

<fieldset>
    <legend>{l s='Настройки' mod='orderadmin'}</legend>
    <form action="{$action}" method="post" enctype="multipart/form-data" id="form">
        <table id="orderadmin_configuration">
            <tr>
                <td colspan="2"><b>{l s='Минимални системни изисквания за работа на модула за доставка:' mod='orderadmin'}</b></td>
            </tr>
            {foreach from=$requirements item=requirment}
                <tr>
                    <td style="float: right;">{$requirment.name}{if isset($requirment.required)} ({$requirment.required}){/if}</label></td>
                    <td><span style="color: {if $requirment.is_success}green{else}red{/if}">{if isset($requirment.current)}{$requirment.current} {/if}{if $requirment.is_success}{l s='Изпълнено' mod='orderadmin'}{else}{l s='НЕ Е ИЗПЪЛНЕНО' mod='orderadmin'}{/if}<span></td>
                </tr>
            {/foreach}
            <tr>
                <td><label for="orderadmin_version">{l s='Версия на модула за доставка:' mod='orderadmin'}</label></td>
                <td><span>{$orderadmin_version}<span></td>
            </tr>
            <tr>
                <td><span class="orderadmin_required"></span> <label for="orderadmin_server_address">{l s='Адрес на сървъра:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" id="orderadmin_server_address" name="orderadmin_server_address" value="{$orderadmin_server_address}" />
                    {if $error_server}
                        <span class="orderadmin_error">{$error_server}</span>
                    {/if}
                </td>
            </tr>
            <tr>
                <td><span class="orderadmin_required"></span> <label for="orderadmin_username">{l s='Потребителско име:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" id="orderadmin_username" name="orderadmin_username" value="{$orderadmin_username}" />
                    {if $error_username}
                        <span class="orderadmin_error">{$error_username}</span>
                    {/if}
                </td>
            </tr>
            <tr>
                <td><span class="orderadmin_required"></span> <label for="orderadmin_password">{l s='Парола:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" onclick="$(this).val('');" id="orderadmin_password" name="orderadmin_password" value="{$orderadmin_password}" />
                    {if $error_password}
                        <span class="orderadmin_error">{$error_password}</span>
                    {/if}
                </td>
            </tr>
            <tr>
                <td><span class="orderadmin_required"></span> <label for="orderadmin_owner">{l s='Id на потребителя:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" onclick="$(this).val('');" id="orderadmin_owner" name="orderadmin_owner" value="{$orderadmin_owner}" />
                    {if $error_owner}
                        <span class="orderadmin_error">{$error_owner}</span>
                    {/if}
                </td>
            </tr>
            <tr>
                <td><span class="orderadmin_required"></span> <label for="orderadmin_sender">{l s='Id на изпращача:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" onclick="$(this).val('');" id="orderadmin_sender" name="orderadmin_sender" value="{$orderadmin_sender}" />
                    {if $error_sender}
                        <span class="orderadmin_error">{$error_sender}</span>
                    {/if}
                </td>
            </tr>
            <tr>
                <td><span class="orderadmin_required"></span> <label for="orderadmin_default_shop">{l s='Id на магазина в Orderadmin:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" onclick="$(this).val('');" id="orderadmin_default_shop" name="orderadmin_default_shop" value="{$orderadmin_default_shop}" />
                    {if $error_shop}
                        <span class="orderadmin_error">{$orderadmin_default_shop}</span>
                    {/if}
                </td>
            </tr>
            <tr>
                <td><span class="orderadmin_required"></span> <label for="orderadmin_default_source">{l s='Id на източника:' mod='orderadmin'}</label></td>
                <td>
                    <input type="text" onclick="$(this).val('');" id="orderadmin_default_source" name="orderadmin_default_source" value="{$orderadmin_default_source}" />
                    {if $error_source}
                        <span class="orderadmin_error">{$orderadmin_default_source}</span>
                    {/if}
                </td>
            </tr>
            <tr>
                <td><label for="orderadmin_secret">{l s='Модулен ключ (попълва се авоматично):' mod='orderadmin'}</label></td>
                <td>
                    <input style="background-color : #d1d1d1;cursor: pointer;" type="text" id="orderadmin_secret" name="orderadmin_secret" value="{$orderadmin_secret}" readonly/>
                </td>
            </tr>
            {if $orderadmin_password}

                <tr>
                    <td><h3 style="text-align: right">{l s='Използвай този модул при:' mod='orderadmin'}</h3></td>
                </tr>

                <tr>
                    <td><label for="actionValidateOrder">{l s='Нова Поръчка:' mod='orderadmin'}</label></td>
                    <td>
                        <input class="ajaxInput" data-config="ORDERADMIN_HOOK_ORDER" type="checkbox" id="actionValidateOrder" name="orderadmin_hook_order" value="{$orderadmin_hook_order}" {if $orderadmin_hook_order == 1}checked{/if} />
                    </td>
                </tr>

                <tr>
                    <td><label for="actionOrderEdited">{l s='Редактиране на поръчка:' mod='orderadmin'}</label></td>
                    <td>
                        <input class="ajaxInput" data-config="ORDERADMIN_HOOK_ORDER_EDIT" type="checkbox" id="actionOrderEdited" name="orderadmin_hook_order_edit" value="{$orderadmin_hook_order_edit}" {if $orderadmin_hook_order_edit == 1}checked{/if} />
                    </td>
                </tr>

                <tr>
                    <td><label for="actionOrderStatusPostUpdate">{l s='Смяна статуса на поръчка:' mod='orderadmin'}</label></td>
                    <td>
                        <input class="ajaxInput" data-config="ORDERADMIN_HOOK_ORDER_STATUS" type="checkbox" id="actionOrderStatusPostUpdate" name="orderadmin_hook_order_status" value="{$orderadmin_hook_order_status}" {if $orderadmin_hook_order_status == 1}checked{/if} />
                    </td>
                </tr>

                <tr>
                    <td><label for="actionProductSave">{l s='Запазване на продукти:' mod='orderadmin'}</label></td>
                    <td>
                        <input class="ajaxInput" data-config="ORDERADMIN_HOOK_PRODUCT" type="checkbox" id="actionProductSave" name="orderadmin_hook_product" value="{$orderadmin_hook_product}" {if $orderadmin_hook_product == 1}checked{/if} />
                    </td>
                </tr>

                <tr>
                    <td><label for="actionAdminAddressSave">{l s='Редактиране на адрес на получател на поръчка:' mod='orderadmin'}</label></td>
                    <td>
                        <input class="ajaxInput" data-config="ORDERADMIN_HOOK_ADDRESS" type="checkbox" id="actionAdminAddressSave" name="orderadmin_hook_address" value="{$orderadmin_hook_address}" {if $orderadmin_hook_address == 1}checked{/if} />
                    </td>
                </tr>

            {/if}

        </table>
        <input type="hidden" value="1" name="form_submitted" />
    </form>
</fieldset>

<br />
<br />

<fieldset>
    <div class="buttons">
        <a onclick="$('#form :input').removeAttr('disabled');
            $('#form').submit();" class="button">
            <span>{l s='Запиши' mod='orderadmin'}</span>
        </a>
        {if $orderadmin_password}
            <a style="margin-left: 10px" onclick="importData(); return false;" id="orderadmin_check_credentials" class="button">{l s='Импортване на данни' mod='orderadmin'}</a>
        {/if}
    </div>
</fieldset>

<script type="text/javascript">
    {literal}
    function importData() {
        $.ajax(
            {
                url: 'index.php?controller=AdminModules&configure=orderadmin&token={/literal}{$token}{literal}&tab_module=shipping_logistics&module_name=orderadmin&ajax_function=import_data',
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    if (response && response.errors.length) {

                        for (i = 0; i < response.errors.length; ++i) {
                            alert(response.errors[i]);
                        }

                        alert('Check your credentials and try again');

                    } else {

                        alert(response.success);

                    }
                }
            }
        );
    }

    $('input[type="checkbox"]').change(function () {

        var ajax_function = '';
        var hook = $(this).attr('id');
        var config = $(this).attr('data-config');
        var msg = '';

        if ($(this).is(':checked')) {
            ajax_function = 'register_hook';
            msg = 'Успешно включихте тази фунционалност';
        } else {
            ajax_function = 'unregister_hook';
            msg = 'Успешно изключихте тази фунционалност';
        }

        $.ajax(
            {
                url: 'index.php?controller=AdminModules&configure=orderadmin&token={/literal}{$token}{literal}&tab_module=shipping_logistics&module_name=orderadmin&ajax_function=' + ajax_function + '&hook=' + hook + '&config=' + config,
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    alert(msg);
                }
            }
        );


    });
    {/literal}
</script>
