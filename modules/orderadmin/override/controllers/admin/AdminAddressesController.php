<?php

/**
 *
 * @property Address $object
 */
class AdminAddressesController extends AdminAddressesControllerCore
{
    public function processSave()
    {
        $parent = get_parent_class(AdminAddressesControllerCore::class);

        if (Tools::getValue('submitFormAjax')) {
            $this->redirect_after = false;
        }
        if (Validate::isEmail(Tools::getValue('email'))) {
            $customer = new Customer();
            $customer->getByEmail(Tools::getValue('email'), null, false);
            if (Validate::isLoadedObject($customer)) {
                $_POST['id_customer'] = $customer->id;
            } else {
                $this->errors[] = $this->trans('This email address is not registered.', array(), 'Admin.Orderscustomers.Notification');
            }
        } elseif ($id_customer = Tools::getValue('id_customer')) {
            $customer = new Customer((int) $id_customer);
            if (Validate::isLoadedObject($customer)) {
                $_POST['id_customer'] = $customer->id;
            } else {
                $this->errors[] = $this->trans('This customer ID is not recognized.', array(), 'Admin.Orderscustomers.Notification');
            }
        } else {
            $this->errors[] = $this->trans('This email address is not valid. Please use an address like bob@example.com.', array(), 'Admin.Orderscustomers.Notification');
        }
        if (Country::isNeedDniByCountryId(Tools::getValue('id_country')) && !Tools::getValue('dni')) {
            $this->errors[] = $this->trans('The identification number is incorrect or has already been used.', array(), 'Admin.Orderscustomers.Notification');
        }

        $id_state = (int) Tools::getValue('id_state');
        $id_country = (int) Tools::getValue('id_country');
        $country = new Country((int) $id_country);
        if ($country && !(int) $country->contains_states && $id_state) {
            $this->errors[] = $this->trans('You have selected a state for a country that does not contain states.', array(), 'Admin.Orderscustomers.Notification');
        }

        if ((int) $country->contains_states && !$id_state) {
            $this->errors[] = $this->trans('An address located in a country containing states must have a state selected.', array(), 'Admin.Orderscustomers.Notification');
        }
        $postcode = Tools::getValue('postcode');

        if ($country->zip_code_format && !$country->checkZipCode($postcode)) {
            $this->errors[] = $this->trans('Your Zip/postal code is incorrect.', array(), 'Admin.Notifications.Error') . '<br />' . $this->trans('It must be entered as follows:', array(), 'Admin.Notifications.Error') . ' ' . str_replace('C', $country->iso_code, str_replace('N', '0', str_replace('L', 'A', $country->zip_code_format)));
        } elseif (empty($postcode) && $country->need_zip_code) {
            $this->errors[] = $this->trans('A Zip/postal code is required.', array(), 'Admin.Notifications.Error');
        } elseif ($postcode && !Validate::isPostCode($postcode)) {
            $this->errors[] = $this->trans('The Zip/postal code is invalid.', array(), 'Admin.Notifications.Error');
        }
        /* If this address come from order's edition and is the same as the other one (invoice or delivery one)
        ** we delete its id_address to force the creation of a new one */
        if ((int) Tools::getValue('id_order')) {
            $this->_redirect = false;
            if (isset($_POST['address_type'])) {
                $_POST['id_address'] = '';
                $this->id_object = null;
            }
        }
        $address = new Address();
        $this->errors = array_merge($this->errors, $address->validateFieldsRequiredDatabase());
        $return = false;
        if (empty($this->errors)) {
            $return = $parent::processSave();
        } else {
            $this->display = 'edit';
        }

        $address_type = (int) Tools::getValue('address_type') == 2 ? 'invoice' : 'delivery';

        if ($this->action == 'save' && ($id_order = (int) Tools::getValue('id_order')) && !count($this->errors) && !empty($address_type)) {
            if (!Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'orders SET `id_address_' . bqSQL($address_type) . '` = ' . (int) $this->object->id . ' WHERE `id_order` = ' . (int) $id_order)) {
                $this->errors[] = $this->trans('An error occurred while linking this address to its order.', array(), 'Admin.Orderscustomers.Notification');
            } else {
                $order = new Order($id_order);
                $order->refreshShippingCost();
                Hook::exec('actionAdminAddressSave', [$return, 'id_order' => $id_order]);
                Tools::redirectAdmin(urldecode(Tools::getValue('back')) . '&conf=4');
            }
        }
        return $return;
    }
}
