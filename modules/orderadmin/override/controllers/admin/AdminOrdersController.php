<?php
/*
* module: orderadmin
* date: 2019-11-11 16:13:05
* version: 1.0.0
*/

class AdminOrdersController extends AdminOrdersControllerCore
{
    public function ajaxProcessGetLocality() {
        $order_details = new Order((int)Tools::getValue('order_id'));
        $address_details = new Address((int)($order_details->id_address_delivery));
        $orderadmin = Module::getInstanceByName('orderadmin');
        $cityString = $address_details->city;
        $searchNeedles = ['/гр.\s/','/гр\s/','/гр./','/гр/','/Гр.\s/','/Гр./','/Гр/','/ГР.\s/','/ГР./','/ГР/','/ГР\s/'];
        $city = preg_replace($searchNeedles,'',$cityString);

        $locality = $orderadmin->getData(
            'api/locations/localities',
            'localities',
            [
                'filter' => [
                    [
                        'field' => 'country',
                        'type' => 'eq',
                        'value' => 25
                    ],
                    [
                        'field' => 'name',
                        'type' => 'ilike',
                        'value' => $city
                    ]
                ]
            ]
        );

        if(!empty($locality[0])) {
            $data = [
                'locality' => [
                    'id' => $locality[0]['id'],
                    'name' => $locality[0]['name'],
                    'postcode' => $locality[0]['postcode'],
                ]
            ];
        } else {
            $data = [
                'error' => 'Error'
            ];
        }

        die(
        Tools::jsonEncode(
            $data
        )
        );
    }

    public function ajaxProcessGetServicePoints() {
        $order_details = new Order((int)Tools::getValue('order_id'));
        $address_details = new Address((int)($order_details->id_address_delivery));
        $orderadmin = Module::getInstanceByName('orderadmin');
        $cityString = $address_details->city;
        $searchNeedles = ['/гр.\s/','/гр\s/','/гр./','/гр/','/Гр.\s/','/Гр./','/Гр/','/ГР.\s/','/ГР./','/ГР/','/ГР\s/'];
        $city = preg_replace($searchNeedles,'',$cityString);

        $locality = $orderadmin->getData(
            'api/locations/localities',
            'localities',
            [
                'filter' => [
                    [
                        'field' => 'country',
                        'type' => 'eq',
                        'value' => 25
                    ],
                    [
                        'field' => 'name',
                        'type' => 'ilike',
                        'value' => $city
                    ]
                ]
            ]
        );

        $oaServicePoints = $orderadmin->getData(
            'api/delivery-services/service-points',
            'servicePoints',
            [
                'filter' => [
                    [
                        'field' => 'deliveryService',
                        'type' => 'eq',
                        'value' => 23
                    ],
                    [
                        'field' => 'locality',
                        'type' => 'eq',
                        'value' => $locality[0]['id']
                    ]
                ]
            ]
        );

        $servicePoints = [];
        foreach ($oaServicePoints as $point) {
            if (!empty($point['rawAddress'])){
                $addr =  sprintf(
                    '%s - %s',
                    $point['name'],
                    $point['rawAddress']
                );
            } else {
                $addr =  sprintf(
                    '%s - %s',
                    $point['name'],
                    $point['raw']['address']['localAddressString']
                );
            }
            $servicePoints[$point['id']] = [
                'extId' => $point['extId'],
                'address' => $addr
            ];
        }

        if(!empty($locality[0]) && !empty($servicePoints)) {
            $data = [
                'servicePoints' => $servicePoints,
                'locality' => [
                    'id' => $locality[0]['id'],
                    'name' => $locality[0]['name'],
                    'postcode' => $locality[0]['postcode'],
                ]
            ];

            if (Module::isInstalled('speedy')) {
                $sqlSpeedyInfo = sprintf(
                    'SELECT `data` FROM %sspeedy_order WHERE order_id = %s',
                    _DB_PREFIX_,
                    (int)Tools::getValue('order_id')
                );

                $queryResult = Db::getInstance()->getValue($sqlSpeedyInfo);

                $speedyData = unserialize($queryResult);

                if (!empty($speedyData["office_id"])) {
                    $data['speedySelected'] = $speedyData["office_id"];
                }
            }
        } else {
            $data = [
                'error' => 'Error'
            ];
        }

        die(
        Tools::jsonEncode(
            $data
        )
        );
    }

    public function ajaxProcessCreateDeliveryRequest() {
        $orderadmin = Module::getInstanceByName('orderadmin');
        $sql = sprintf(
            '(SELECT %sconfiguration.value FROM %sconfiguration WHERE name = "%s") UNION (SELECT %sconfiguration.value FROM %sconfiguration WHERE name = "%s") UNION (SELECT %sconfiguration.value FROM %sconfiguration WHERE name = "%s")',
            _DB_PREFIX_,
            _DB_PREFIX_,
            trim('ORDERADMIN_DEFAULT_SHOP'),
            _DB_PREFIX_,
            _DB_PREFIX_,
            trim('ORDERADMIN_OWNER'),
            _DB_PREFIX_,
            _DB_PREFIX_,
            trim('ORDERADMIN_SENDER')
        );
        $sqlRes = Db::getInstance()->ExecuteS($sql);
        $shopId = (int)$sqlRes[0]['value'];
        $ownerId = (int)$sqlRes[1]['value'];
        $senderId = (int)$sqlRes[2]['value'];
        $oa_order_id = (int)Tools::getValue('oa_order_id');
        $order_id    = (int)Tools::getValue('order_id');
        $point_id    = (int)Tools::getValue('point_id');
        $point_addr  = Tools::getValue('point_addr');
        $loc_name    = Tools::getValue('loc_name');
        $loc_id    = Tools::getValue('loc_id');
        $loc_pc      = (int)Tools::getValue('loc_pc');
        $trackingNumber      = Tools::getValue('trackingNumber');

        $order = new Order((int) $order_id);
        $customer = new Customer((int)$order->id_customer);
        $address = new Address((int)$order->id_address_delivery);
        $oaOrderProducts = $orderadmin->getData(
            'api/products/order/product',
            'order_product',
            [
                'filter' => [
                    [
                        'field' => 'order',
                        'type' => 'eq',
                        'value' => $oa_order_id
                    ],
                ]
            ]
        );
        $items = [];
        if (!empty($oaOrderProducts)) {
            foreach ($oaOrderProducts as $product) {
                $items[] = [
                    'name'          => $product['_embedded']['productOffer']['name'],
                    'count'         => $product['count'],
                    'extId'         => $product['id'],
                    'weight'        => $product['_embedded']['productOffer']['weight'],
                    'payment'       => $product['total'],
                    'estimatedCost' => $product['total']
                ];
            }
        }
        $deliveryRequestData = [
            'sender'            => $senderId,
            'extId'             => $oa_order_id,
            'clientExtId'       => $order_id,
            'places'            => [
                [
                    'items' => $items
                ]
            ],
            'estimatedCost'     => $order->total_paid,
            'payment'           => $order->total_paid,
            'recipientPhone'    => !empty(trim($address->phone)) ? $address->phone : $address->phone_mobile,
            'recipientLocality' => $loc_id,
            'trackingNumber'    => !empty($trackingNumber) ? $trackingNumber : null,
            'recipient'         => [
                'extId'  => $customer->id,
                'name'   => $customer->firstname . ' ' . $customer->lastname,
                'email'  => $customer->email,
                'phone'  => !empty(trim($address->phone)) ? $address->phone : $address->phone_mobile
            ],
            'recipientAddress'  => [
                'locality' => [
                    'country' => 25,
                    'name' => $loc_name
                ],
                'postcode' => $loc_pc,
                'notFormal' => !empty($point_addr) ? $point_addr : $address->address1 . ', ' . $address->address2
            ],
            'deliveryService'   => 23,
            'servicePoint'      => $point_id,
            'currency'          => 2,
            'rate'              => [
                'id' => 239
            ],
            'eav'               => [
                'delivery-request-products-order' => $oa_order_id
            ]
        ];
        $drId = $orderadmin->postToOrderadmin(
            $deliveryRequestData,
            'api/delivery-services/requests',
            $orderadmin::TYPE_POST
        );
        if(!empty($drId)) {
            $orderData = [
                'eav' => [
                    'delivery-services-request' => $drId
                ]
            ];
            $orderadmin->postToOrderadmin(
                $orderData,
                'api/products/order/' . (int)$oa_order_id,
                $orderadmin::TYPE_PATCH
            );
        }
        if(!empty($drId)) {
            $data = [
                'drId' => $drId
            ];
        } else {
            $data = [
                'error' => 'Error'
            ];
        }
        die(
        Tools::jsonEncode(
            $data
        )
        );
    }
}