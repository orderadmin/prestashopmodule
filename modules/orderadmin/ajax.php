<?php
/* SSL Management */
$useSSL = true;

require_once(dirname(__FILE__).'../../../config/config.inc.php');
require_once(dirname(__FILE__).'../../../config/smarty.config.inc.php');
require_once(dirname(__FILE__).'../../../init.php');
include_once(dirname(__FILE__).'/orderadmin.php');

$orderadmin = new Orderadmin();

if (Tools::getValue('ajax_function') == 'get_offices') {
    $data = $_POST;
    $orderadmin->getOffices($data);
} elseif (Tools::getValue('ajax_function') == 'set_orderadmin_method') {
    $orderadmin->setOrderadminMethod();
}

?>