<?php

$sql = array();

//$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_countries_list` (
//		  `orderadmin_country_id` INT(11) NOT NULL AUTO_INCREMENT,
//		  `orderadmin_country_extId` VARCHAR(255) NOT NULL DEFAULT '',
//		  `orderadmin_country_name` VARCHAR(255) NOT NULL DEFAULT '',
//		  PRIMARY KEY (`orderadmin_country_id`)
//		) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";
//
//$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_localities` (
//		  `orderadmin_locality_id` INT(11) NOT NULL AUTO_INCREMENT,
//		  `orderadmin_locality_extId` VARCHAR(255) NOT NULL DEFAULT '',
//		  `orderadmin_locality_postcode` VARCHAR(255) NOT NULL DEFAULT '',
//		  `orderadmin_locality_name` VARCHAR(255) NOT NULL DEFAULT '',
//		  `orderadmin_country_code` VARCHAR(255) NOT NULL DEFAULT '',
//		  PRIMARY KEY (`orderadmin_locality_id`)
//		) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";
//
//$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_shops` (
//		  `orderadmin_shop_id` INT(11) NOT NULL AUTO_INCREMENT,
//		  `orderadmin_shop_extId` VARCHAR(255) NOT NULL DEFAULT '',
//		  `orderadmin_shop_name` VARCHAR(255) NOT NULL DEFAULT '',
//		  PRIMARY KEY (`orderadmin_shop_id`)
//		) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";

//$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_orders` (
//		  `orderadmin_order_id` INT(11) NOT NULL AUTO_INCREMENT,
//		  `order_id` INT(11) NOT NULL DEFAULT '0',
//		  `order_extId` INT(11) NULL,
//		  `address` TEXT NOT NULL,
//		  `postcode` VARCHAR(10) NOT NULL DEFAULT '',
//		  `city` VARCHAR(255) NOT NULL DEFAULT '',
//		  `courier` VARCHAR(255) NOT NULL DEFAULT '',
//		  `deliver_to` VARCHAR(255) NOT NULL DEFAULT '',
//		  PRIMARY KEY (`orderadmin_order_id`),
//		  KEY `order_id` (`order_id`)
//		) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";

//$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_senders` (
//		  `orderadmin_sender_id` INT(11) NOT NULL AUTO_INCREMENT,
//		  `orderadmin_sender_extId` VARCHAR(255) NOT NULL DEFAULT '',
//		  `orderadmin_sender_name` VARCHAR(255) NOT NULL DEFAULT '',
//		  `orderadmin_sender_locality` VARCHAR(255) NOT NULL DEFAULT '',
//		  `orderadmin_sender_sending_postcode` VARCHAR(255) NOT NULL DEFAULT '',
//		  PRIMARY KEY (`orderadmin_sender_id`)
//		) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_orders` (
		  `orderadmin_order_id` INT(11) NOT NULL AUTO_INCREMENT,
		  `order_id` INT(11) NOT NULL,
		  `order_extId` INT(11) NULL DEFAULT 0,
		  `dr_id` INT(11) NULL,
		  `order_status` VARCHAR(255) NOT NULL DEFAULT 'Заредена в Orderadmin',
		  PRIMARY KEY (`orderadmin_order_id`),
		  UNIQUE KEY `order_id` (`order_id`)
		) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_products` (
		  `orderadmin_product_id` INT(11) NOT NULL AUTO_INCREMENT,
		  `product_id` VARCHAR(255) NOT NULL,
		  `product_extId` VARCHAR(255) NULL,
		  `oa_product_type` VARCHAR(255) NULL,
		  `product_status` VARCHAR(255) NOT NULL DEFAULT 'Зареден в Orderadmin',
		  PRIMARY KEY (`orderadmin_product_id`),
		  UNIQUE KEY `product_id` (`product_id`)
		) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_orders_monitor` (
		  `orderadmin_order_monitor_id` INT(11) NOT NULL AUTO_INCREMENT,
		  `state_id` INT(11) NOT NULL,
		  `orderadmin_state` VARCHAR(255) NULL,
		  PRIMARY KEY (`orderadmin_order_monitor_id`),
		  UNIQUE KEY `state_id` (`state_id`)
		) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_awaiting` (
		  `orderadmin_awaiting_id` INT(11) NOT NULL AUTO_INCREMENT,
		  `attr_id` INT(11) NOT NULL,
		  `product_id` INT(11) NOT NULL,
		  PRIMARY KEY (`orderadmin_awaiting_id`),
		  UNIQUE KEY `attr_id` (`attr_id`)
		) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_order_products_awaiting` (
        `orderadmin_order_products_awaiting_id` INT(11) NOT NULL AUTO_INCREMENT,
        `type` VARCHAR(255) NOT NULL,
        `data` LONGTEXT NOT NULL,
        `result` LONGTEXT NULL,
        `execution_time` DATETIME NULL,
        PRIMARY KEY (`orderadmin_order_products_awaiting_id`)
        ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8;";

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "orderadmin_export_states` (
		  `orderadmin_export_state_id` INT(11) NOT NULL AUTO_INCREMENT,
		  `state_id` INT(11) NOT NULL,
		  PRIMARY KEY (`orderadmin_export_state_id`)
		) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";