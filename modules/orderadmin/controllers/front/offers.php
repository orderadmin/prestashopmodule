<?php

require_once _PS_MODULE_DIR_ . 'orderadmin/cron/oaOffersUpdate.php';

class OrderadminOffersModuleFrontController extends ModuleFrontController
{
    //CRON
    public function initContent()
    {
        $oaOrders = new OaOffersUpdate();
        $oaOrders->initContent();
    }
}