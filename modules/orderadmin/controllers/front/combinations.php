<?php

require_once _PS_MODULE_DIR_ . 'orderadmin/cron/oaCombinations.php';

class OrderadminCombinationsModuleFrontController extends ModuleFrontController
{
    //CRON
    public function initContent()
    {
        $combinations = new OaCombinations();
        $combinations->initContent();
    }
}