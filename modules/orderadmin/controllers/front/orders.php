<?php

require_once _PS_MODULE_DIR_ . 'orderadmin/cron/oaOrders.php';

class OrderadminOrdersModuleFrontController extends ModuleFrontController
{
    //CRON
    public function initContent()
    {
        $oaOrders = new OaOrders();
        $oaOrders->initContent();
    }
}