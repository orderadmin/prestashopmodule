<?php

class OrderadminApiModuleFrontController extends ModuleFrontControllerCore
{
    public function initContent()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            http_response_code(403);
            $return = [
                'message' => 'Access denied'
            ];

            die(
                Tools::jsonEncode(
                    $return
                )
            );
        } else if (Configuration::get('ORDERADMIN_SECRET') != $_SERVER['PHP_AUTH_PW']) {
            http_response_code(405);
            $return = [
                'message' => 'Wrong module secret'
            ];

            die(
                Tools::jsonEncode(
                    $return
                )
            );
        }

        $data = json_decode(file_get_contents('php://input'), true);

        if (json_last_error() === 0 ) {

            if (empty($data['action'])) {
                $return = [
                    'message' => 'Action not set'
                ];

                die(
                    Tools::jsonEncode(
                        $return
                    )
                );
            }

            switch ($data['action']) {
                case 'update_order_status':
                    $this->updateOrderStatus($data);
                    break;

                case 'update_product':
                    $this->updateProduct($data);
                    break;

                case 'get_offers':
                    $this->getOffers($data);
                    break;

                default:
                    $return = [
                        'message' => sprintf(
                            'Unknown action %s',
                            $data['action']
                        )
                    ];

                    die(
                    Tools::jsonEncode(
                        $return
                    )
                    );
            }

        } else {
            http_response_code(400);
            $return = [
                'message' => json_last_error_msg()
            ];

            die(
                Tools::jsonEncode(
                    $return
                )
            );
        }
    }

    public function updateOrderStatus(array $data) {

        if (empty($data['order_id']) || empty($data['order_status_id'])) {
            $return = [
                'message' => 'Order ID or order status ID not set'
            ];
        } else {

            $objOrder = new Order((int)$data['order_id']);

            if (!empty($objOrder->id)) {

                if ((int)$objOrder->current_state
                    != (int)$data['order_status_id']
                ) {
                    $history = new OrderHistory();
                    $history->id_order = (int)$objOrder->id;
                    $history->changeIdOrderState(
                        (int)$data['order_status_id'], (int)$objOrder->id
                    );
                    $history->save();

                    $sql = sprintf(
                        'SELECT `name` FROM `' . _DB_PREFIX_
                        . 'order_state_lang` WHERE `id_lang` = 1 AND `id_order_state` = %s',
                        (int)$data['order_status_id']
                    );

                    $stateName = Db::getInstance()->getValue($sql);

                    $return = [
                        'message' => sprintf(
                            'Order state changed to - %s',
                            $stateName
                        )
                    ];
                } else {
                    $sql = sprintf(
                        'SELECT `name` FROM `' . _DB_PREFIX_
                        . 'order_state_lang` WHERE `id_lang` = 1 AND `id_order_state` = %s',
                        (int)$data['order_status_id']
                    );

                    $stateName = Db::getInstance()->getValue($sql);

                    $return = [
                        'message' => sprintf(
                            'Current order state is same as the new state - %s',
                            $stateName
                        )
                    ];
                }
            } else {
                http_response_code(404);
                $return = [
                    'message' => sprintf(
                        'Order with ID: %s not found',
                        $data['order_id']
                    )
                ];
            }
        }

        die(
            Tools::jsonEncode(
                $return
            )
        );
    }

    public function updateProduct(array $data) {

        if(empty($data['type']) || empty($data['product_id'])) {
            $return = [
                'message' => 'Product type or product ID not set'
            ];
        } else {

            switch ($data['type']) {
                case 'product':
                    $product = new Product((int)$data['product_id'], false);
                    if (!empty($product->id)) {
                        if (!empty($data['fields'])) {
                            foreach ($data['fields'] as $key => $value) {
                                $product->$key = $value;
                            }
                            $product->save();
                            $return = [
                                'message' => sprintf(
                                    'Fields updated for product ID: %s',
                                    $product->id
                                )
                            ];
                        } else {
                            $return = [
                                'message' => sprintf(
                                    'Not fields set to update for product ID: %s',
                                    $product->id
                                )
                            ];
                        }
                    } else {
                        http_response_code(404);
                        $return = [
                            'message' => sprintf(
                                'No product with ID: %s found',
                                $data['product_id']
                            )
                        ];
                    }
                    break;
                case 'combination':
                    $comb = new Combination($data['product_id']);
                    if(!empty($comb->id)) {
                        if (!empty($data['fields']['ean13'])) {
                            if ($comb->ean13 != $data['fields']['ean13']) {
                                $comb->ean13 = $data['fields']['ean13'];
                                $comb->save();
                                $return = [
                                    'message' => sprintf(
                                        'ean13 updated for combination ID: %s',
                                        $comb->id
                                    )
                                ];
                            } else{
                                $return = [
                                    'message' => sprintf(
                                        'ean13 (%s) of combination ID: %s is same as the new ean13 (%s)',
                                        $comb->ean13,
                                        $comb->id,
                                        $data['fields']['ean13']
                                    )
                                ];
                            }
                        } else {
                            $return = [
                                'message' => 'Only ean13 can be updated for combinations. Field not set'
                            ];
                        }
                    } else {
                        http_response_code(404);
                        $return = [
                            'message' => sprintf(
                                'No combination with ID: %s found',
                                $data['product_id']
                            )
                        ];
                    }
                    break;
                default:
                    $return = [
                        'message' => 'Unknown product type'
                    ];
                    break;
            }
        }
        die(
            Tools::jsonEncode(
                $return
            )
        );
    }

    public function getOffers(array $data) {
        $limit  = $data['limit'];
        $offset = $data['offset'];

        $sql = sprintf(
            'SELECT p.id_product,
                pl.name AS "name",
                p.price AS "price",
                p.wholesale_price AS "wholesale_price",
                p.ean13 AS "ean13",
                p.weight AS "weight",
                p.reference AS "reference",
                p.date_add AS "date_created",
                p.date_upd AS "date_updated",
                GROUP_CONCAT(pa.id_product_attribute SEPARATOR ",") as "attr_id",
                CONCAT(
                    \'https://www.baby.bg/img/p/\',
                    IF(CHAR_LENGTH(pi.id_image) >= 7, CONCAT(SUBSTRING(pi.id_image, -7, 1), \'/\'), \'\'),
                    IF(CHAR_LENGTH(pi.id_image) >= 6, CONCAT(SUBSTRING(pi.id_image, -6, 1), \'/\'), \'\'),
                    IF(CHAR_LENGTH(pi.id_image) >= 5, CONCAT(SUBSTRING(pi.id_image, -5, 1), \'/\'), \'\'),
                    IF(CHAR_LENGTH(pi.id_image) >= 4, CONCAT(SUBSTRING(pi.id_image, -4, 1), \'/\'), \'\'),
                    IF(CHAR_LENGTH(pi.id_image) >= 3, CONCAT(SUBSTRING(pi.id_image, -3, 1), \'/\'), \'\'),
                    if(CHAR_LENGTH(pi.id_image) >= 2, CONCAT(SUBSTRING(pi.id_image, -2, 1), \'/\'), \'\'),
                    IF(CHAR_LENGTH(pi.id_image) >= 1, CONCAT(SUBSTRING(pi.id_image, -1, 1), \'/\'), \'\'),
                    pi.id_image, 
                    \'.jpg\'
                ) as "image_url"
                FROM %sproduct p
                LEFT JOIN %sproduct_lang pl ON p.id_product = pl.id_product AND id_shop = 1 and id_lang = 1
                LEFT JOIN %sproduct_attribute pa ON p.id_product = pa.id_product and id_lang = 1
                LEFT JOIN %simage pi ON p.id_product = pi.id_product AND cover = 1
                WHERE p.date_upd > DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                AND p.active = 1
                GROUP BY p.id_product LIMIT %s OFFSET %s',
            _DB_PREFIX_,
            _DB_PREFIX_,
            _DB_PREFIX_,
            _DB_PREFIX_,
            $limit,
            $offset
        );

        $offers = Db::getInstance()->ExecuteS($sql);

        if (empty($offers)) {
            $return = [
                'message' => sprintf(
                    'No products have been found with params limit %s and offset %s, updated since %s',
                    $limit,
                    $offset,
                    date(
                        'Y-m-d h:m:s',
                        strtotime('- 1 day')
                    )
                )
            ];

            die(
            Tools::jsonEncode(
                $return
            )
            );
        }

        foreach ($offers as $key => $offer) {
            if(empty($offer['attr_id'])) {
                continue;
            }

            $comb = Db::getInstance()->executeS('
                SELECT pac.id_product_attribute, 
                        pa.price as "combination_price",
                        pa.ean13 as "combination_ean13",
                        pa.weight as "combination_weight",
                        pa.reference as "combination_reference",
                        pa.wholesale_price as "combination_wholesale_price",
                        GROUP_CONCAT(agl.`name`, " - ",al.`name` ORDER BY agl.`id_attribute_group` SEPARATOR " - ") as combination_name
                FROM `' . _DB_PREFIX_ . 'product_attribute_combination` pac
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute` a ON a.`id_attribute` = pac.`id_attribute`
                LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa ON pa.`id_product_attribute` = pac.`id_product_attribute`
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group` ag ON ag.`id_attribute_group` = a.`id_attribute_group`
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = 1)
                LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = 1)
                WHERE pac.id_product_attribute IN (' . $offer['attr_id'] . ')
                GROUP BY pac.id_product_attribute');

            $offers[$key]['combinations'] = $comb;

        }

        die(
            Tools::jsonEncode(
                $offers
            )
        );
    }
}