{if ($ps_version == '1.6') OR ($ps_version == '1.7')}
<div class="col-lg-6">
    <div class="panel">
        <div class="panel-heading">
            <span><img src="{$this_path}oa_lit.png" /> {l s='Информация за продукта' mod='orderadmin'}</span>
        </div>
        <div id="orderadmin" class="well hidden-print">
{else}
<fieldset>
{/if}
    <table class="orderadmin_generate" id="orderadmin_tabel">
        <tr>
            <td><label for="product_id">{l s='ID на продукта:' mod='orderadmin'}</label></td>
            <td>
                <input type="text" id="product_id" name="product_id" disabled="disabled" value="{$product_id}" />
            </td>
        </tr>

        <tr>
            <td><label for="oa_product_id">{l s='ID на продукта в Orderadmin:' mod='orderadmin'}</label></td>
            <td>
                <input type="text" id="oa_product_id" name="oa_product_id" disabled="disabled" value="{$oa_product_id}" />
                <br />
            </td>
        </tr>

        <tr>
            <td><label for="oa_product_type">{l s='Тип на продукта в Orderadmin:' mod='orderadmin'}</label></td>
            <td>
                <input type="text" id="oa_product_type" name="oa_product_type" disabled="disabled" value="{$oa_product_type}" />
                <br />
            </td>
        </tr>

        <tr>
            <td><label for="product_state">{l s='Статус на продукта:' mod='orderadmin'}</label></td>
            <td>
                <input type="text" id="product_state" name="product_state" disabled="disabled" value="{$product_state}" />
                <br />
            </td>
        </tr>
    </table>
{if ($ps_version == '1.6') OR ($ps_version == '1.7')}
        </div>
    </div>
</div>
{else}
</fieldset>
{/if}